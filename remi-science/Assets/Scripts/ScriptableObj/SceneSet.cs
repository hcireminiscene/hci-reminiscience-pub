﻿using UnityEngine;
using System.Collections;

/// <summary>
/// SceneSet SriptableObj - Simple Obj used in editor to create multiple "Scenes" in the Game.
/// </summary>
public class SceneSet : ScriptableObject
{
	[System.Serializable]
	public class AnimationSpriteSheet
	{
		public Sprite[] spriteSheet;
	}
	[System.Serializable]
	public struct AnimationTransformData
	{
		public TransformData startTransform;
		public TransformData endTransform;
	}

	[Header ("Completion State")]
	[SerializeField] private bool 			completed;
	[SerializeField] private byte[]			starAward;			

	[Header ("Puzzle Animation")]
	[SerializeField] private AudioClip[]			puzzleSounds;
	[SerializeField] private AnimationSpriteSheet[]		puzzleAnimation;

	[SerializeField] private AnimationTransformData[]	transformData;
	[SerializeField] private uint[]				puzzleAudioCue;
	
	[Header ("Object Puzzle")]
	[SerializeField] private Texture2D[]			objectPuzzleImage;
	[SerializeField] private Sprite[] 			objectPuzzleAnimationBackground;


	[Header ("Scene Puzzle")]
	[SerializeField] private Quaternion[]		sceneRotation;
	[SerializeField] private Vector3[]		sceneScale;
	[SerializeField] private Vector3[]		scenePlacementPosition;

	[SerializeField] private bool[]			scenePuzzlePlayAnimation;
	[SerializeField] private uint[]			scenePuzzleAnimationOrder;
	[SerializeField] private Sprite			scenePuzzleImage;

	public bool	Completed			
	{ 	
			set { completed = value; } 	
			get { return completed;	 }
										
	}
	public byte[]	StarAward
	{ 	
		set { starAward = value; } 	
		get { return starAward;	 }
	}

	public bool[] ScenePuzzlePlayAnimation			{	get { return scenePuzzlePlayAnimation;		}	}
	public uint[]	PuzzleAudioCue				{  	get { return puzzleAudioCue;			}	} 
	public Sprite[] SpriteAnimationBackground		{   	get { return objectPuzzleAnimationBackground;	}	} 
	public AnimationSpriteSheet[] PuzzleAnimation		{  	get { return puzzleAnimation;			}	} 
	public AnimationTransformData[]	TransformData		{ 	get { return transformData;			} 	}
	public AudioClip[]	PuzzleSounds			{	get { return puzzleSounds;			}	}
	public Texture2D[]	ObjectPuzzleImage 		{	get { return objectPuzzleImage; 		}	}
	public Sprite 		ScenePuzzleImage 		{	get { return scenePuzzleImage; 			}	}
	public uint[]		ScenePuzzleAnimationOrder 	{	get { return scenePuzzleAnimationOrder; 	} 	}
	public Vector3[]	PositionList			{ 	get { return scenePlacementPosition; 		} 	}
	public Vector3[]	ScaleList			{ 	get { return sceneScale;			} 	}
	public Quaternion[]	RotationList			{ 	get { return sceneRotation;			} 	}

	public void Reset()
	{
		completed = false;
		for(int i=0;i<starAward.Length;i++)
		{
			starAward[i] = 0;
		}
	}

	//BUBBLE SORT for quickie sort
	public void SortAnimationInList()
	{
		foreach(AnimationSpriteSheet animation in puzzleAnimation)
		{
			if(animation.spriteSheet.Length != 0)
			{
				for(int k=0;k<animation.spriteSheet.Length;k++)
				{
					for(int i=0;i<animation.spriteSheet.Length;i++)
					{
						if(i+1<animation.spriteSheet.Length)
						{
							if(string.Compare(animation.spriteSheet[i].name,animation.spriteSheet[i+1].name) > 0)
							{
								Sprite temp =  animation.spriteSheet[i];
								animation.spriteSheet[i] = animation.spriteSheet[i+1];
								animation.spriteSheet[i+1] = temp;
								
							}
						}
					}
				}
			}
		}
	}

	public void ResetTransformData()
	{
		for(int i=0;i< transformData.Length;i++)
		{
			transformData[i].endTransform.position	= transformData[i].startTransform.position	= Vector3.zero;
			transformData[i].endTransform.rotation	= transformData[i].startTransform.rotation	= Quaternion.identity;
			transformData[i].endTransform.scale	= transformData[i].startTransform.scale		= Vector3.one;
		}
	}
}
