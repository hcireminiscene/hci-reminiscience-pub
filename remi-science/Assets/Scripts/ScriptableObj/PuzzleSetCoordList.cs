﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Scriptable PuzzleSetCoordinates, Another obj in Editor to convert csvFile to usable data for the Object Puzzle Piece Coordinates.
/// </summary>
public class PuzzleSetCoordList : ScriptableObject
{	
	[SerializeField] private Rect			dimension;
	[SerializeField] private TextAsset[] 		coordinateCSV; 
	[SerializeField] private PuzzleSetCoord[]	puzzleSetCoordList;
	[SerializeField] private TriangleList[]		triangleList;

	public void GenerateData()
	{
		puzzleSetCoordList = new PuzzleSetCoord[coordinateCSV.Length];
		for(int i=0;i<coordinateCSV.Length;i++)
		{
			puzzleSetCoordList[i].dimension = dimension;

			if(coordinateCSV[i] == null) throw new System.MissingFieldException("Missing CSV File in element: " + i);

			string[,] data = CSVReader.SplitCsvGrid(coordinateCSV[i].ToString());
			puzzleSetCoordList[i].numOfPiece = data.GetUpperBound(1);
			puzzleSetCoordList[i].piece =  new Piece[data.GetUpperBound(1)];
			for(int col=0;col<data.GetUpperBound(1);col++)
			{
				if(data[0,col] == "Coordinates")
				{
					int numOfVertices=0;
					for(int row=1;data[row,col]!=""&&data[row,col]!=null;row++)
					{
						numOfVertices++;
					}

					Vector2[] vertices = new Vector2[numOfVertices];

					for(int row=1;row-1<numOfVertices;row++)
					
					{
						Debug.Log(data[row,col]);
						string xValue = "",yValue = "";
						bool flag = false;
						foreach(char c in data[row,col])
						{
							if(c == '-')	
								flag = true;
							else
							{
								if(!flag)	xValue+=c;
								else 		yValue+=c;
							}
						}
						vertices[row-1] = new Vector2(float.Parse( xValue ),float.Parse( yValue ));
						Debug.Log(vertices[row-1]);
					}

					puzzleSetCoordList[i].piece[col].vertCoord = vertices;

					if(vertices.Length == 3)		puzzleSetCoordList[i].piece[col].vertTriangles = triangleList[0].triangles;
					else if(vertices.Length == 4)		puzzleSetCoordList[i].piece[col].vertTriangles = triangleList[1].triangles;
					else if(vertices.Length == 5)		puzzleSetCoordList[i].piece[col].vertTriangles = triangleList[2].triangles;
				}
			}
		}
	}

	public PuzzleSetCoord RandomCoordinate(byte _numOfPiece)
	{
		List<PuzzleSetCoord> temp = new List<PuzzleSetCoord>();
		for(int i=0;i<puzzleSetCoordList.Length;i++)
		{
			if(puzzleSetCoordList[i].numOfPiece == _numOfPiece)
			{
				temp.Add(puzzleSetCoordList[i]);
			}
		}
		return temp.GetRandom();
	}

	public PuzzleSetCoord[] PuzzleSetCoord
	{
		get { return puzzleSetCoordList; }
	}


}