﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

/// <summary>
/// ScriptableObject for Adjust difficulty chart data. Obj in editor used to convert csvFile into data that we can use for adjusting difficulty
/// </summary>
public class AdjustDifficultyChartData : ScriptableObject 
{
	[SerializeField] private TextAsset csvFile;
	[SerializeField] public AdjustDifficultyChart[] generatedChart;


	public void GenerateData()
	{
		string[,] data = CSVReader.SplitCsvGrid(csvFile.ToString());

		Debug.Log(data.GetUpperBound(1));
		generatedChart = new AdjustDifficultyChart[data.GetUpperBound(1)-1];
		for(int i=1;i<data.GetUpperBound(1);i++)
		{
			generatedChart[i-1].numOfPiece		= int.Parse( data[0,i] );
			generatedChart[i-1].chart.decreaseChart	= byte.Parse( data[1,i] );
			generatedChart[i-1].chart.remainChart	= byte.Parse( data[2,i] );
			generatedChart[i-1].chart.increaseChart	= byte.Parse( data[3,i] );
		}

	}
}
