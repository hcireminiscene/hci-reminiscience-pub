﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Sprite packer tagger - use for tagging texture2d/sprites that we want to be repacked by Unity Sprite Packer.
/// </summary>
public class SpritePackerTagger : ScriptableObject
{
	[SerializeField] public string 		tag;
	[SerializeField] public Texture2D[] 	list;

#if UNITY_EDITOR || UNITY_EDITOR_OSX
	public UnityEditor.BuildTarget[] targets;
#endif
	public string Tag
	{
		get { return tag; }
	}

	public Texture2D[] List
	{
		get { return list;}
	}

}