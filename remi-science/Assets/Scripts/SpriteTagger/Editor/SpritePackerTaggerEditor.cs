﻿using UnityEngine;
using UnityEditor;
/// <summary>
/// Editor for SpritePacker, yet again buttons for our Sprite Packer Tool, mainly use to manage our sprites so that we know which sprites have been tagged to be packed by a specific group
/// </summary>
[CustomEditor (typeof(SpritePackerTagger))]
public class SpritePackerTaggerEditor : Editor
{

	[MenuItem("Assets/Create Sprite Packer Tagger")]
	public static void CreateAsset ()
	{
		ScriptableObjectUtility.CreateAsset<SpritePackerTagger>();

	}

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		SpritePackerTagger obj =  (SpritePackerTagger)target;
		if(GUILayout.Button("Tagg"))
		{
			for(int i=0;i<obj.list.Length;i++)
			{
				if(obj.list[i] != null)
				{
					int id = obj.list[i].GetInstanceID();
					
					TextureImporter textureImporter = AssetImporter.GetAtPath(AssetDatabase.GetAssetPath(id)) as TextureImporter;
					textureImporter.spritePackingTag = obj.tag;
					textureImporter.SaveAndReimport();
				}

			}
		}
		if(GUILayout.Button("Repack"))
		{
			for(int i=0;i<obj.targets.Length;i++)
			{
				if(EditorUtility.DisplayDialog("Repack Build","Do you want to repack buid type: " + obj.targets[i].ToString(),"Yes","No"))
					UnityEditor.Sprites.Packer.RebuildAtlasCacheIfNeeded(obj.targets[i],true);
			}
		}
	}

}