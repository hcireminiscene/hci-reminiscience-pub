﻿using UnityEngine;
using UnityEditor;
using System.Collections;


/// <summary>
/// Editor for SceneSet Scriptable Object. Just more buttons for us to generate the data properly. again make life easier so that we can create SceneSets in Editor
/// </summary>
[CustomEditor(typeof(SceneSet)),CanEditMultipleObjects]
public class SceneSetsEditor : Editor
{	
	[MenuItem("Assets/Create SceneSet")]
	public static void CreateAsset ()
	{
		ScriptableObjectUtility.CreateAsset<SceneSet>();
	}

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		SceneSet myScript = (SceneSet)target;
		if(GUILayout.Button("Reset Complete State"))	
		{
			myScript.Reset();
			EditorUtility.SetDirty(myScript);
		}
		if(GUILayout.Button("Sort Animation"))			
		{
			myScript.SortAnimationInList();
			EditorUtility.SetDirty(myScript);
		}
		if(GUILayout.Button("Reset TransformData"))	
		{
			myScript.ResetTransformData();
			EditorUtility.SetDirty(myScript);
		}

	}
}
