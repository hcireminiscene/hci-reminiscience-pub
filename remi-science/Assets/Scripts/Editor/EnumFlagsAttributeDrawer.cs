﻿using UnityEngine;
using UnityEditor;
using System.Collections;

/// <summary>
/// Editor for EnumFlag Attributes. Taken from https://github.com/xfleckx/SNEED/blob/master/Editor/Util/EnumFlagsAttributeDrawer.cs , simple to draw the EnmFlag in the Editor
/// </summary>
[CustomPropertyDrawer (typeof (EnumFlagAttribute))]
public class EnumFlagsAttributeDrawer : PropertyDrawer
{
	public override void OnGUI (Rect _position, SerializedProperty _property, GUIContent _label) 
	{
		// for dropdown box
		//_property.intValue = EditorGUI.MaskField( _position, _label, _property.intValue, _property.enumNames );

		// for buttons
		int buttonsIntValue = 0;
		int enumLength = _property.enumNames.Length;
		bool[] buttonPressed = new bool[enumLength];
		float buttonWidth = (_position.width - EditorGUIUtility.labelWidth) / enumLength;
		
		EditorGUI.LabelField(new Rect(_position.x, _position.y, EditorGUIUtility.labelWidth, _position.height), _label);
		
		EditorGUI.BeginChangeCheck ();
		
		for (int i = 0; i < enumLength; i++) 
		{
			
			// Check if the button is/was pressed 
			if ( ( _property.intValue & (1 << i) ) == 1 << i ) {
				buttonPressed[i] = true;
			}
			
			Rect buttonPos = new Rect (_position.x + EditorGUIUtility.labelWidth + buttonWidth * i, _position.y, buttonWidth, _position.height);
			
			buttonPressed[i] = GUI.Toggle(buttonPos, buttonPressed[i], _property.enumNames[i],  "Button");
			
			if (buttonPressed[i])
				buttonsIntValue += 1 << i;
		}
		
		if (EditorGUI.EndChangeCheck()) 
		{
			_property.intValue = buttonsIntValue;
		}
	}
}
