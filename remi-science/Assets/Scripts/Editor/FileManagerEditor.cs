﻿using UnityEngine;
using UnityEditor;
using System.Collections;

/// <summary>
/// Editor for FileManager, there to have a button to clear data from our PC! As we will always be creating csv files whenever we RUN the game.
/// </summary>
[CustomEditor(typeof(FileManager))]
public class FileManagerEditor : Editor
{
	
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		if(FileManager.playerIndex != 0 && !Application.isPlaying)
		{
			if(GUILayout.Button("Delete Folders"))
			{
				ClearFolders(FileManager.playerIndex);
				PlayerPrefs.DeleteAll();
				FileManager.playerIndex = 0;
				FileManager.fileIndex   = 0;
			}
		}
	}

	public static void ClearFolders(int _length)
	{
		for(int i=0;i<_length;i++)
		{
			if(System.IO.Directory.Exists(Application.persistentDataPath + "/Player " + i.ToString()))
			{
				System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo( Application.persistentDataPath + "/Player " + i.ToString() );
				foreach(System.IO.FileInfo file in dir.GetFiles())
				{
					file.Delete();
				}
				System.IO.Directory.Delete(Application.persistentDataPath + "/Player " + i.ToString());
			}
			else
				EditorUtility.DisplayDialog("Folder Not Found",Application.persistentDataPath + "/Player " + i.ToString(),"OK");
		}
		
		EditorUtility.DisplayDialog("Success!","Folders Deleted!","OK");
	}
}

