﻿using UnityEngine;
using UnityEditor;
using System.Collections;

/// <summary>
/// Editor for PuzzleSetCoordinator, simply a button to generate the data and letting us create a PuzzleSetCoordinator scriptable object.
/// </summary>
[CustomEditor(typeof(PuzzleSetCoordList))]
public class PuzzleSetCoordListEditor : Editor
{
	
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		
		PuzzleSetCoordList myScript = (PuzzleSetCoordList)target;
		if(GUILayout.Button("Generate Data"))
		{
			myScript.GenerateData();
			EditorUtility.SetDirty(myScript);
		}
	}

	[MenuItem("Assets/Create PuzzleSetCoordList")]
	public static void CreateAsset ()
	{
		ScriptableObjectUtility.CreateAsset<PuzzleSetCoordList>();
	}
}

