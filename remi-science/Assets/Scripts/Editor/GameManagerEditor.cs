﻿using UnityEngine;
using UnityEditor;
using System.Collections;

/// <summary>
/// Editor for GameManager. Just another button, this time to reset all scene that we have played as the data in 
/// our Scriptable Obj SceneSet checked whenever we played 1 of the scene. Typically used this only when we are going to push builds into phone or build APKs
/// </summary>
[CustomEditor(typeof(GameManager))]
public class GameManagerEditor : Editor 
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		
		GameManager myScript = (GameManager)target;
		if(GUILayout.Button("Reset Scene Sets"))
		{
			myScript.ResetSceneSet();
			EditorUtility.SetDirty(myScript);
		}
	}

}