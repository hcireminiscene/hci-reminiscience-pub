﻿using UnityEditor;
using UnityEngine;
using System.Collections;

/// <summary>
/// Editor Script for AdjustDifficultyChart, just there to have a button in order to generate our data, as well as letting us create an AdjustDifficultyChartData Obj in Editor from Assets/Create Difficulty Chart
/// </summary>
[CustomEditor (typeof(AdjustDifficultyChartData))]
public class AdjustDifficultyChartDataEditor : Editor 
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		AdjustDifficultyChartData myScript = (AdjustDifficultyChartData)target;
		if(GUILayout.Button("Generate Data"))
		{
			myScript.GenerateData();
			EditorUtility.SetDirty(myScript);
		}
	}
	
	[MenuItem("Assets/Create Difficulty Chart")]
	public static void CreateAsset ()
	{
		ScriptableObjectUtility.CreateAsset<AdjustDifficultyChartData>();
	}
}
