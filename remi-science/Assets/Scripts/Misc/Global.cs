using UnityEngine;
using System.Collections;

[System.Serializable]
public struct PuzzleSetCoord
{
	public int		numOfPiece;
	public Rect 		dimension;		// dimension of the puzzle set
	public Piece[]		piece;			// pieces	
}

[System.Serializable]
public struct Piece
{
	public Vector2[] vertCoord;	// must be within dimension setting
	public ushort[]	 vertTriangles;	// vertice triangle
}

[System.Serializable]
public struct TriangleList
{
	public ushort[]	triangles;
}

[System.Serializable]
public struct Rotation
{
	public Vector3 lowRange;
	public Vector3 highRange;
}

[System.Flags] 
public enum ParticleSubEmitterType	
{ 
	OnBirth		= 1 << 0,	
	OnDeath		= 1 << 1,
	OnCollision 	= 1 << 2
};

[System.Serializable]
public struct Vector3Path
{
	public	Vector3[] node;
};

[System.Serializable]
public struct HintTiming
{
	public string name;
	public float idleNotPlaceTiming;
	public float touchNotPlaceTiming;
}

[System.Serializable]
public struct TransformData
{
	public Vector3 		position;
	public Quaternion 	rotation;
	public Vector3		scale;
}


[System.Serializable] 
public struct AdjustDifficultyChart
{
	[TooltipAttribute("Number of pieces for this difficulty")] public int numOfPiece;
	[TooltipAttribute("Number of pieces to change")] public DifficultyChart chart;
};

public enum RotationType { low,mid,high };
public enum gameState { splashScreen, objectPuzzle, scenePuzzle, levelSelect };

[System.Serializable]
public struct PieceTiming
{
	public int		index;
	public System.TimeSpan	idleTime;
	public System.TimeSpan 	completeTime;
	public int 		hintsRecieved;
}


[System.Serializable] public enum PlayerState 		{ idle, touchNotMove };
[System.Serializable] public enum PlayerDifficult 	{ low, mid, high };

[System.Serializable] 
public struct DifficultyChart
{
	public byte decreaseChart;
	public byte remainChart;
	public byte increaseChart;
};

[System.Serializable]
public enum GameMode { release, autoSnap }