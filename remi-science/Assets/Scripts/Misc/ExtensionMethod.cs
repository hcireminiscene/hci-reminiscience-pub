﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// All our ExtensionMethods used
/// </summary>
public static class ExtensionMethod 
{
	/// <summary>
	/// Convert Vector3 to Vector2
	/// </summary>
	public static Vector2 Vec2(this Vector3 _vec3)
	{
		return new Vector2(_vec3.x,_vec3.y);
	}

	/// <summary>
	/// Lerp the specified _transfrom, _transformData, _elpase and _space.
	/// </summary>
	/// <param name="_transfrom">_transfrom.</param>
	/// <param name="_transformData">_transform data.</param>
	/// <param name="_elpase">_elpase.</param>
	/// <param name="_space">_space.</param>
	public static void Lerp(this Transform _transfrom, SceneSet.AnimationTransformData _transformData,float _elpase,Space _space)
	{
		if(_space == Space.Self)
		{
			_transfrom.localPosition = Vector3.Lerp(_transformData.startTransform.position,_transformData.endTransform.position,_elpase);
			_transfrom.localScale	 = Vector3.Lerp(_transformData.startTransform.scale,_transformData.endTransform.scale,_elpase);
			_transfrom.localRotation = Quaternion.Lerp(_transformData.startTransform.rotation,_transformData.endTransform.rotation,_elpase);
		}
		else
		{
			_transfrom.position	 = Vector3.Lerp(_transformData.startTransform.position,_transformData.endTransform.position,_elpase);
			_transfrom.localScale	 = Vector3.Lerp(_transformData.startTransform.scale,_transformData.endTransform.scale,_elpase);
			_transfrom.rotation	 = Quaternion.Lerp(_transformData.startTransform.rotation,_transformData.endTransform.rotation,_elpase);

		}
		
	}

	/// <summary>
	/// Lerp the specified _transfrom, _startTran, _endTran, _elpase and _space. (NOT TESTED)
	/// </summary>
	/// <param name="_transfrom">_transfrom.</param>
	/// <param name="_startTran">_start tran.</param>
	/// <param name="_endTran">_end tran.</param>
	/// <param name="_elpase">_elpase.</param>
	/// <param name="_space">_space.</param>
	public static void Lerp(this Transform _transfrom, Transform _startTran, Transform _endTran,float _elpase,Space _space)
	{
		if(_space == Space.Self)
		{
			_transfrom.localPosition = Vector3.Lerp(_startTran.localPosition,_endTran.localPosition,_elpase);
			_transfrom.localScale	 = Vector3.Lerp(_startTran.localScale,_endTran.localScale,_elpase);
			_transfrom.localRotation = Quaternion.Lerp(_startTran.localRotation,_endTran.localRotation,_elpase);
		}
		else
		{
			_transfrom.position 	 = Vector3.Lerp(_startTran.localPosition,_endTran.localPosition,_elpase);
			_transfrom.localScale	 = Vector3.Lerp(_startTran.localScale,_endTran.localScale,_elpase);
			_transfrom.rotation	 = Quaternion.Lerp(_startTran.localRotation,_endTran.localRotation,_elpase);
		}

	}

	public static Vector3 ChangeZValue(this Vector3 _vec3,float _zValue)
	{
		return new Vector3(_vec3.x,_vec3.y,_zValue);
	}


	public static T GetRandom<T>(this IList<T> _list)
	{
		System.Random random = new System.Random();
		if(_list.Count == 0)
		{
			throw new System.Exception("Empty List");
		}
		return _list[random.Next(0,_list.Count)];
	}

	public static Quaternion Zero(this Quaternion _quaternion)
	{
		return new Quaternion(0.0f,0.0f,0.0f,0.0f);
	}

	public static AudioSource Audio(this GameObject _gameObject)
	{
		return _gameObject.GetComponent<AudioSource>();
	}

	public static void Clear(this System.Text.StringBuilder _stringBuilder)
	{
		_stringBuilder.Length = 0;
		_stringBuilder.Capacity = 0;
	}
}