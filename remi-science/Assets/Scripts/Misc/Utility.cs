﻿using UnityEngine;

/// <summary>
/// Simple Utility Class for us to use.
/// </summary>
public static class Utility
{
	public static Vector3 RandomRange(Vector3 _lowRange,Vector3 _highRange)
	{
		return new Vector3
			(
				Random.Range(_lowRange.x,_highRange.x),
				Random.Range(_lowRange.y,_highRange.y),
				Random.Range(_lowRange.z,_highRange.z)
				);
	}

	public static Vector2 RotatePoint(float _angle, Vector2 _point)
	{ 
		float radian = _angle * Mathf.PI / 180.0f;
		float cosa = Mathf.Cos(radian), sina = Mathf.Sin(radian);
		return new Vector2((_point.x * cosa) - (_point.y * sina), (_point.y * cosa) + (_point.x * sina));
	}

	public static void ShuffleArray<T>(T[] arr) 
	{
		for (int i = arr.Length - 1; i > 0; i--) 
		{
			int r = Random.Range(0, i);
			T tmp = arr[i];
			arr[i] = arr[r];
			arr[r] = tmp;
		}
	}
}