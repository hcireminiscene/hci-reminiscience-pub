using UnityEngine;
using System.Collections;

/// <summary>
/// Code to vibrate iOS or Android devices
/// </summary>
public static class Vibration
{
	#region GameObjectExtensionMethods
	public static void Vibrate(this GameObject _go)
	{
		Vibrate();
	}
	public static void Vibrate(this GameObject _go,float _seconds)
	{
		Vibrate(_seconds);
	}
	#endregion

	public static void Vibrate()
	{
		Handheld.Vibrate();
	}

	public static void Vibrate(float _seconds)
	{
		Handheld.Vibrate();
	}
	
	public static void Vibrate(long _milliseconds)
	{
		Handheld.Vibrate();
	}
	
	public static void Vibrate(long[] _pattern, int _repeat)
	{
		Handheld.Vibrate();
	}

}