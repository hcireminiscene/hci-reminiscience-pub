﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Simple vibration component to use in inspector
/// </summary>
public class VibrateComponent : MonoBehaviour 
{
	public void Vibrate()
	{
		Vibration.Vibrate();
	}
	public void Vibrate(float _time)
	{
		Vibration.Vibrate(_time);
	}
}
