﻿using UnityEngine;

/// <summary>
/// Enum flag attribute. Simple PropertyAttribute for Unity Editor (so that we can define EnumFlags and see them as Attribute in Editor as well as manipulating them in the Editor)
/// </summary>
public class EnumFlagAttribute : PropertyAttribute
{
	public EnumFlagAttribute() { }
}
