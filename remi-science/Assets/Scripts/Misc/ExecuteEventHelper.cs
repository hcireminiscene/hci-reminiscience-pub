﻿using UnityEngine.EventSystems;

/// <summary>
/// Custom Event Helper for adding new interface
/// </summary>
public static class ExecuteEventHelper
{
	private static void Execute(IOnWaitHandler _waitHandler, BaseEventData _eventData)
	{
		_waitHandler.OnWait(ExecuteEvents.ValidateEventData<PointerEventData>(_eventData));
	}
	
	public static ExecuteEvents.EventFunction<IOnWaitHandler> OnWaitHandler
	{
		get { return Execute; }
	}
}