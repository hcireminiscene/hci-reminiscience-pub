﻿using UnityEngine;
using System.Collections;
using System.Linq; 
using System;

/// <summary>
/// CSV reader. Taken from wiki.unity3d.com/index.php?title=CSVReader Modifed to fit our needs as a static class instead.
/// </summary>
public static class CSVReader
{
	
	// outputs the content of a 2D array, useful for checking the importer
	public static void DebugOutputGrid(string[,] _grid)
	{
		string textOutput = ""; 
		for (int y = 0; y < _grid.GetUpperBound(1); y++) {	
			for (int x = 0; x < _grid.GetUpperBound(0); x++) {
				
				textOutput += _grid[x,y]; 
				textOutput += "|"; 
			}
			textOutput += "\n"; 
		}
		Debug.Log(textOutput);
	}
	
	// splits a CSV file into a 2D string array
	public static string[,] SplitCsvGrid(string _csvText)
	{
		string[] lines = _csvText.Split("\n"[0]); 
		
		// finds the max width of row
		int width = 0; 
		for (int i = 0; i < lines.Length; i++)
		{
			string[] row = SplitCsvLine( lines[i] ); 
			width = Mathf.Max(width, row.Length); 
		}
		
		// creates new 2D string grid to output to
		string[,] outputGrid = new string[width + 1, lines.Length + 1]; 
		for (int y = 0; y < lines.Length; y++)
		{
			string[] row = SplitCsvLine( lines[y] ); 
			for (int x = 0; x < row.Length; x++) 
			{
				outputGrid[x,y] = row[x]; 
				
				// This line was to replace "" with " in my output. 
				// Include or edit it as you wish.
				outputGrid[x,y] = outputGrid[x,y].Replace("\"\"", "\"");
			}
		}
		
		return outputGrid; 
	}
	
	// splits a CSV row 
	public static string[] SplitCsvLine(string line)
	{
		return (from System.Text.RegularExpressions.Match m in System.Text.RegularExpressions.Regex.Matches(line,
		                                                                                                    @"(((?<x>(?=[,\r\n]+))|""(?<x>([^""]|"""")+)""|(?<x>[^,\r\n]+)),?)", 
		                                                                                                    System.Text.RegularExpressions.RegexOptions.ExplicitCapture)
		        select m.Groups[1].Value).ToArray();
	}

	public static void SaveOutputGrid(string[,] grid,string path)
	{
		string textOutput = "";
		int upperBound = grid.GetUpperBound(0) - 1;
		for (int y = 0; y < grid.GetUpperBound(1); y++)
		{
			for (int x = 0; x < grid.GetUpperBound(0); x++)
			{
				
				textOutput += grid[x, y];
				if (x != upperBound)
					textOutput += ",";
			}
			textOutput += Environment.NewLine;
		}
		System.IO.File.WriteAllText(path, textOutput);
	}
}