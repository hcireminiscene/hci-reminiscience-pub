﻿using UnityEngine;
using System.Collections;

using UnityEngine.EventSystems;

/// <summary>
/// Abstract class for all Clickable Puzzle Piece using Unity EventSystem interface
/// </summary>
public abstract class ClickablePieceBase : PuzzlePieceBase,IDragHandler,IPointerUpHandler,IPointerDownHandler,IBeginDragHandler, IOnWaitHandler
{
	[SerializeField] protected PuzzlePieceBase   	placementPiece;
	[SerializeField] protected PolygonCollider2D 	pieceCollider;
	[SerializeField] protected Vector2 		deltaModifier;

	protected Vector2		offScreenOffset;

	[SerializeField] protected AudioClip[]	clips;
	protected AudioSource	audioSource;

	
	[SerializeField] protected float				touchSense;
	[SerializeField] protected float 				checkDistance;
	[SerializeField] protected float 				speed;

	protected PointerEventData pointerEventData;

	protected override void Awake()
	{
		audioSource		= gameObject.Audio();
		pieceCollider 		= GetComponent<PolygonCollider2D>();

		pointerEventData = null;
	}
	
	public PuzzlePieceBase PlacementPiece
	{
		get { return placementPiece; }
		set { placementPiece = value; }
	}


	#region IBeginDragHandler implementation

	public virtual void OnBeginDrag (PointerEventData _eventData)
	{
		if(!isClickable) return;

		transform.rotation = Quaternion.Euler(Vector3.zero);
	}

	#endregion

	#region IOnWaitHandler implementation

	public void OnWait(PointerEventData _eventData)
	{
		if(GameManager.Instance.GameState == gameState.objectPuzzle)
		{
			ObjectPuzzleManager.Instance.PauseHints();
		}
		else if(GameManager.Instance.GameState == gameState.scenePuzzle)
		{
			ScenePuzzleManager.Instance.PauseHints();
		}
	}

	#endregion

	#region IDragHandler implementation
	public virtual void OnDrag (PointerEventData _eventData)
	{
		if(!isClickable) 
		{
			return;
		}
		
		//Vector2 mousePos = Camera.main.ScreenToWorldPoint(_eventData.position );
		if(GameManager.Instance.GameState == gameState.objectPuzzle)
			GameManager.Instance.CurrentPlayerState = PlayerState.touchNotMove;
		
		// move the piece
		transform.Translate( Camera.main.ScreenToViewportPoint( new Vector2(_eventData.delta.x*deltaModifier.x,_eventData.delta.y*deltaModifier.y) )  );

		if(GameManager.Instance.GameMode == GameMode.autoSnap)
		{
			if((placementPiece.transform.position.Vec2() - this.transform.position.Vec2()).magnitude < checkDistance)
			{
				
				transform.rotation 	= Quaternion.Euler(Vector3.zero);
				transform.position 	= placementPiece.transform.position + (Vector3.back/10);
				isClickable 		= false;

				audioSource.clip = clips[Random.Range(2,3)];
				audioSource.volume = 1.0f;
				audioSource.Play();

				Complete();			
			}
		}

	}
	#endregion

	#region IPointerDownHandler implementation

	public virtual void OnPointerDown (PointerEventData _eventData)
	{
		if(!isClickable) return;
	
		GameManager.Instance.CurrentPlayerState = PlayerState.touchNotMove;


		if(pointerEventData == null)
		{
			pointerEventData = _eventData;
		}
		
		if(pointerEventData != _eventData)
		{
			CustomTouchInputModule inputModule = (CustomTouchInputModule)EventSystem.current.currentInputModule;
			pointerEventData.pointerPress	= null;
			pointerEventData.pointerDrag	= null;
			pointerEventData.pointerEnter	= null;

			_eventData.pointerId = 0;
			pointerEventData = _eventData;
		}


		StopCoroutine("Translate");
		Vibration.Vibrate(0.1f);
		transform.rotation = Quaternion.Euler(Vector3.zero);

		if(isClickable)
		{
			GameData.Instance.RecordIdleData();

			audioSource.clip = clips[0];
			audioSource.Play();
		}


	}

	#endregion


	#region IPointerUpHandler implementation

	public virtual void OnPointerUp (PointerEventData _eventData)
	{
		if(!isClickable) 	
			return;		

		GameManager.Instance.CurrentPlayerState = PlayerState.idle;

		if(pointerEventData != null)
		{
			pointerEventData = null;
		}
	

//		Debug.Log("<color=red>"+(placementPiece.transform.position.Vec2() - this.transform.position.Vec2()).magnitude+"</color>");

		if((placementPiece.transform.position.Vec2() - this.transform.position.Vec2()).magnitude > checkDistance)
		{
			//UIManager.Instance.UIDebug.text = "";
			Rect limit = GameManager.Instance.PlayAreaLimit;
			float x=0;
			float y=0;

			if(GameManager.Instance.GameState == gameState.objectPuzzle)
				transform.rotation 	= ObjectPuzzleManager.Instance.LowRotation();
			
			foreach(Vector2 vert in image.sprite.vertices)
			{
				Vector2 temp = Utility.RotatePoint(ObjectPuzzleManager.Instance.CurrentRotation.z, vert);
				temp = new Vector2(temp.x + transform.position.x,temp.y+transform.position.y);
				
				if( !limit.Contains(temp) )
				{
					if(temp.x > limit.xMax)			x = (Mathf.Abs(x) < Mathf.Abs(limit.xMax - temp.x) || x == 0) ? limit.xMax - temp.x : x ; 	// true:false
					else if(temp.x < limit.xMin)	x = (Mathf.Abs(x) < Mathf.Abs(limit.xMin - temp.x) || x == 0) ? limit.xMin - temp.x : x ;	// true:false
					
					if(temp.y > limit.yMax)			y = (Mathf.Abs(y) < Mathf.Abs(limit.yMax - temp.y) || y == 0) ? limit.yMax - temp.y : y ; 	// true:false
					else if(temp.y < limit.yMin)	y = (Mathf.Abs(y) < Mathf.Abs(limit.yMin - temp.y) || y == 0) ? limit.yMin - temp.y : y ;	// true:false
					
					//UIManager.Instance.UIDebug.text += temp.ToString() + "\n";
				}
				offScreenOffset = new Vector2(x,y);
				if(offScreenOffset.magnitude > 0)
					StartCoroutine("Translate");
			}
			
//			Debug.Log("offset: " + offScreenOffset);
		}
		else
		{
			//Debug.Log(checkDistance);

			transform.rotation 	= Quaternion.Euler(Vector3.zero);
			transform.position 	= placementPiece.transform.position + (Vector3.back/10);
			isClickable 		= false;

			Complete();			
		}

		if(isClickable)
		{
			audioSource.clip = clips[1];
			audioSource.volume = 0.5f;
			audioSource.Play();
		}
		else
		{
			audioSource.clip = clips[Random.Range(2,3)];
			audioSource.volume = 1.0f;
			audioSource.Play();
		}

		StopCoroutine("Rotate");
	}

	#endregion

	protected abstract void Complete();

	protected IEnumerator Translate()
	{
		while(true)
		{
			transform.Translate(offScreenOffset.normalized*Time.deltaTime*speed,Space.World);
			offScreenOffset -= offScreenOffset.normalized*Time.deltaTime*speed;
			if(offScreenOffset.magnitude < 1.0f*speed) 
				StopCoroutine("Translate");
			yield return null;
		}
		
	}
}
