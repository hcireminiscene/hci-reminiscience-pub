﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Abstract class for all Timer
/// </summary>
public abstract class TimerBase : MonoBehaviour 
{
	[SerializeField] protected float maxTime;
	[SerializeField] protected float currentTime;
	[SerializeField] protected bool isPause;
	[SerializeField] protected bool useMaxTime;

	protected virtual void Awake()
	{
		ResetTimer();
	}

	public bool IsPause
	{
		get { return isPause;	}
		set { isPause = value;	}
	}

	public abstract void ResetTimer();		

	public float CurrentTime	
	{	
		set	{	currentTime = value;	}
		get {	return currentTime; 	}	
	}
	public float MaxTime 		
	{	
		set		{	maxTime = value;	}
		get		{ 	return maxTime; 	}	
	}
	public float TimeScale	
	{	
		get 
		{ 
			if(useMaxTime)			return (maxTime - currentTime)/maxTime; 
			else 					return currentTime;
		}
	}

	public delegate void TimerEndDelegate();
	public abstract event TimerEndDelegate TimerEnd;				
	public delegate void TimerRunningDelegate(float _elapse);
	public abstract event TimerRunningDelegate TimerRunning;

}
