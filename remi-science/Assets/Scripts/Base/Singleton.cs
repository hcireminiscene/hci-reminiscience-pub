﻿using UnityEngine;

/// <summary>
/// Base for Singleton Managers (Where T is a type of MonoBehaviour)
/// </summary>
public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
	protected static T instance;
	protected static object lockObj = new object();

	[SerializeField] protected bool dontDestroy;

	/// <summary>
	/// Get Singleton static instance.
	/// </summary>
	/// <value>instance</value>
	public static T Instance
	{
		get
		{
			if (applicationIsQuitting) return null;

			lock(lockObj)
			{
				if (instance == null)
				{
					instance = (T) FindObjectOfType(typeof(T));
					
					if ( FindObjectsOfType(typeof(T)).Length > 1 )
						return instance;
					
				}
				return instance;
			}	
		}
	}


	/// <summary>
	/// Default Awake for Singleton Classes.
	/// </summary>
	protected virtual void Awake()
	{
		if(instance == null)				instance = (T)this.GetComponent<T>();		// for being a lazy person
		if(instance != (T)this.GetComponent<T>())	Destroy(this.gameObject);			// destroy game object

		if(dontDestroy)					DontDestroyOnLoad(this.gameObject);		// for special cases
	}

	
	private static bool applicationIsQuitting = false;
	/// <summary>
	/// destroy instance when application is quiting
	/// </summary>
	public void OnDestroy() 
	{
		// removed for testing
		applicationIsQuitting = true;

		// clear static variable
		instance = null;
	}

	protected void OnEnable()
	{
		applicationIsQuitting = false;
	}
}