﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Abstract class for all Puzzle (object and scene puzzles)
/// </summary>
public abstract class PuzzleBase : MonoBehaviour 
{
	[SerializeField] protected SlideTo	slideTo;
	protected bool puzzleCompleted;

	protected virtual void Awake()
	{
		slideTo = GetComponent<SlideTo>();
		slideTo.ReachPosition += SlideReach;
	}

	public bool PuzzleCompleted
	{
		get { return puzzleCompleted;	}
		set { puzzleCompleted = value;	}
	}

	public SlideTo SlideTo
	{
		get { return slideTo; }
	}

	public void SlidePuzzle()
	{
		slideTo.Slide();
	}


	public virtual void SlideReach()
	{
		UIManager.Instance.SlideOut(0);
	}
}
