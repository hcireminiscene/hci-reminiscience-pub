﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Abstract class for all hints
/// </summary>
public abstract class HintBase : MonoBehaviour 
{
	protected bool started;

	public virtual void ResumeHint()  
	{
		started = true;
	}
	public virtual void PauseHint()
	{
		started = false;
	}
	public virtual void ClearHint()
	{
		started = false;
	}
	public virtual void StartHint(ClickablePieceBase _puzzlePiece)
	{
		gameObject.Audio().Play();
		started = true;
	}
}