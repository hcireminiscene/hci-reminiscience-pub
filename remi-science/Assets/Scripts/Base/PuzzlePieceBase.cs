using UnityEngine;
using System.Collections;

/// <summary>
/// Abstract class for all puzzle pieces for the puzzle
/// </summary>
public abstract class PuzzlePieceBase : MonoBehaviour 
{
	[SerializeField] protected SpriteRenderer 		image;

	[SerializeField] protected bool isClickable = false;

	protected virtual void Awake()
	{
		image		= GetComponent<SpriteRenderer>();
	}

	public SpriteRenderer Image
	{
		get { return image; }
	}

	public Light Light
	{
		get { return GetComponent<Light>(); }
	}

	public bool IsClickable
	{
		get { return isClickable;  }
		set { isClickable = value; }
	}
}