﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Base Class for all sprites with animation
/// </summary>
public class SpriteAnimation : MonoBehaviour
{
	[SerializeField] protected Sprite[]		spriteSheet;
	[SerializeField] protected CountdownTimer	timer;

	protected SpriteRenderer			spriteRenderer;

	[SerializeField] protected bool 	loop;
	protected float	animationIndex;
	protected int	animationCurrentIndex;

	// Use this for initialization
	protected virtual void Awake() 
	{
		spriteRenderer		= GetComponent<SpriteRenderer>();
		timer				= GetComponent<CountdownTimer>();

		timer.TimerRunning	+= TimerRunning;
		timer.TimerEnd 		+= TimerEnd;

		spriteRenderer.sprite = spriteSheet[0];
	}

	protected void TimerRunning(float _elapse)
	{
		if((animationCurrentIndex+1)*animationIndex < _elapse)
		{
			if(animationCurrentIndex+1 < spriteSheet.Length)
				animationCurrentIndex++;
			
			spriteRenderer.sprite = spriteSheet[animationCurrentIndex];
		}
	}

	protected void TimerEnd()
	{
		if(loop)
		{
			timer.ResetTimer();
			animationCurrentIndex = 0;
			spriteRenderer.sprite = spriteSheet[animationCurrentIndex];
			return;
		}
		if(AnimationEnd != null)
		{		
			AnimationEnd();		
		}
		
		timer.enabled = false;
	}

	public CountdownTimer Timer 
	{
		get { return timer; }
	}

	public Sprite[] SpriteSheet
	{
		set { spriteSheet = value; 	}
		get { return spriteSheet; 	}
 	}

	public virtual void StartAnimation()
	{
		if(spriteSheet.Length > 0) 
		{
			animationIndex = 1.0f/spriteSheet.Length;
			animationCurrentIndex = 0;
			timer.enabled = true;
			timer.ResetTimer();
		}

	}

	protected virtual void OnDestroy()
	{
		timer.TimerRunning	-= TimerRunning;
		timer.TimerEnd 		-= TimerEnd;
	}

	public delegate void AnimationEndDelegate();
	public event AnimationEndDelegate AnimationEnd;
}