﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Completion Puzzle Piece, location where Object Puzzle Piece will be placed into
/// </summary>
public class CompletionPiece : PuzzlePieceBase
{
	[SerializeField] protected SpriteRenderer[] shadow;
	/// <summary>
	/// Create Completion Piece based on a sprite
	/// </summary>
	/// <param name="_sprite">_sprite.</param>
	public void CreatePiece(Sprite _sprite)
	{
		image.sprite	= _sprite;				
		foreach(SpriteRenderer s in shadow)
		{
			s.sprite = _sprite;
		}
		transform.position += new Vector3(_sprite.pivot.x/_sprite.pixelsPerUnit,_sprite.pivot.y/_sprite.pixelsPerUnit,0.5f);
	}
}