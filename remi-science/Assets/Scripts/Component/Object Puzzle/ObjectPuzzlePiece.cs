﻿using UnityEngine;

using System.Collections;
using UnityEngine.EventSystems;

/// <summary>
/// Object puzzle piece, 1 clickable for the Object Puzzle
/// </summary>
public class ObjectPuzzlePiece : ClickablePieceBase
{
	
	[SerializeField,Range(0,1)] protected float	checkDistancePercentage;
	[SerializeField] protected float		colliderPercentage;
	[SerializeField] protected SpriteRenderer[] shadow;

	[SerializeField] protected Color startColor;
	[SerializeField] protected Color endColor;
	private WaitTimer	waitTimer;
	[SerializeField] protected Vector2[] 	colliderPoints;


	protected override void Awake()
	{
		base.Awake();
		image 				= GetComponentInChildren<SpriteRenderer>();

		waitTimer = GetComponent<WaitTimer>();
		waitTimer.enabled = false;

		waitTimer.TimerRunning	+= HandleTimerRunning;
		waitTimer.TimerEnd	+= HandleTimerEnd;
	}

	protected void HandleTimerEnd()
	{
		waitTimer.enabled = false;	
		image.material.SetColor("_OverlayColor", Color.Lerp(startColor,endColor,0.0f));
	}

	protected void HandleTimerRunning(float _elapse)
	{
		image.material.SetColor("_OverlayColor", Color.Lerp(startColor,endColor,1-_elapse));	
	}

	protected void OnDestroy()
	{
		waitTimer.TimerRunning	-= HandleTimerRunning;
		waitTimer.TimerEnd		-= HandleTimerEnd;
	}

	/// <summary>
	/// Create a Puzzle Piece and set the collider based on a sprite
	/// </summary>
	/// <param name="_sprite">_sprite.</param>
	public void CreatePiece(Sprite _sprite)
	{
		image.sprite = _sprite;
		foreach(SpriteRenderer s in shadow)
		{
			s.sprite = _sprite;
		}
		shadow[shadow.Length-1].gameObject.SetActive(false);

		//collider.points = _sprite.vertices;
		colliderPoints = new Vector2[_sprite.vertices.Length];
		for(int i=0;i<_sprite.vertices.Length;i++)
		{
			colliderPoints[i] = _sprite.vertices[i] * colliderPercentage;
		}
		pieceCollider.points = colliderPoints;

		UpdateCheckDistance(checkDistancePercentage);
	}
	/// <summary>
	/// Update player input distance
	/// </summary>
	/// <param name="_value">_value.</param>
	public void UpdateCheckDistance(float _value)
	{
		checkDistancePercentage = _value;

		Vector2 center = Vector2.zero;
		for(int i=0;i< colliderPoints.Length;i++)
		{
			center+= colliderPoints[i];
		}
		center /= colliderPoints.Length;

		checkDistance = 0.0f;

		for(int i=0;i<colliderPoints.Length;i++)
		{
			if(checkDistance < (center-colliderPoints[i]).magnitude*_value)
				checkDistance = (center-colliderPoints[i]).magnitude*_value;
		}
	}

	#region implemented abstract members of ClickablePieceBase
	protected override void Complete()
	{
		pieceCollider.enabled  	= false;
		//reset all hints
		image.material.SetColor("_OverlayColor", Color.Lerp(startColor,endColor,0.0f));
		placementPiece.Image.material.SetColor("_OverlayColor", Color.Lerp(startColor,endColor,0.0f));
		Destroy(placementPiece.gameObject);
		ObjectPuzzleManager.Instance.ResetAndClearAllHintTimer();
		ObjectPuzzleManager.Instance.PieceSolved(this);

		waitTimer.enabled = true;
		shadow[shadow.Length-1].gameObject.SetActive(false);

	}
	#endregion

	#region IBeginDragHandler implementation
	public override void OnDrag (PointerEventData _eventData)
	{
		base.OnDrag(_eventData);

		ObjectPuzzleManager.Instance.PauseHints();
	}
	#endregion

	#region IPointerClickHandler implementation
	public override void OnPointerDown (PointerEventData _eventData)
	{
		base.OnPointerDown(_eventData);

		if(!isClickable) return;

		ObjectPuzzleManager.Instance.CurrentBatch.ClickedPiece = this;
		ObjectPuzzleManager.Instance.CurrentBatch.ShiftPuzzlePiecePosition(this);


		ObjectPuzzleManager.Instance.PauseHints();
		shadow[shadow.Length-1].gameObject.SetActive(true);

	}
	#endregion

	#region IPointerUpHandler implementation
	public override void OnPointerUp (PointerEventData _eventData)
	{		
		base.OnPointerUp(_eventData);

		
		shadow[shadow.Length-1].gameObject.SetActive(false);
		ObjectPuzzleManager.Instance.ResumeHints();

		//ObjectPuzzleManager.Instance.CurrentBatch.ClickedPiece = null;
		//not main timer
		ObjectPuzzleManager.Instance.ResetIdleTiming();
	}
	#endregion

	#region IBeginDragHandler implementation

	public override void OnBeginDrag (PointerEventData _eventData)
	{
		base.OnBeginDrag(_eventData);

	}
	#endregion
}
