﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Object Puzzle for the actual puzzle
/// </summary>
public class ObjectPuzzle : PuzzleBase 
{
	[SerializeField] private Texture2D				puzzleImage;
	[SerializeField] private Sprite[]				spriteList;
	[SerializeField] private AudioClip[]			clips;
	[SerializeField] private GameObject				objectAnimationOffsetObject;
	[SerializeField] private PuzzleSpriteAnimation 	objectAnimation;
	[SerializeField] private TransformAnimation		transformAnimation;

	[SerializeField] private SpriteRenderer[] 	 	objectAnimationBackground;
	[SerializeField] private AlbumImage				objectAlbumImage;
	[SerializeField] private SpriteRendererGlow		glow;
	
	[SerializeField] private uint animationAudioCueIndex;

	private bool flipHorizontal;
	private bool flipVertical;

	protected override void Awake()
	{
		base.Awake();
	}
	private void Start()
	{
		objectAnimation.SetAudio(clips[1],animationAudioCueIndex);
		objectAnimation.Timer.TimerRunning += transformAnimation.TimerRunningEvent;
	}

	public void StartGlow()
	{
		glow.StartGlow();
	}

	public void ChangeToAnimation()
	{
		objectAnimationOffsetObject.SetActive(true);
	}


	public void StartAnimation()
	{
		objectAnimation.StartAnimation();

	}

	/// <summary>
	/// Create a set of puzzle based on the Puzzle Coordinates.
	/// </summary>
	/// <param name="_set">_set.</param>
	public void CreateSliceSprite(PuzzleSetCoord _set)
	{
		spriteList	= new Sprite[_set.piece.Length];

		flipHorizontal	= Random.Range(0,2) == 0 ? true : false;
		flipVertical	= Random.Range(0,2) == 0 ? true : false;

		//Debug.Log(flipHorizontal + " : " + flipVertical);
		for(int i=0;i<_set.piece.Length;i++)
		{
			Vector2 center = Vector2.zero;
			Vector2[] vertCoordinate = new Vector2[_set.piece[i].vertCoord.Length];

			for(int j=0;j<_set.piece[i].vertCoord.Length;j++)
			{
				vertCoordinate[j] = _set.piece[i].vertCoord[j];
				if(flipVertical) 
				{
					vertCoordinate[j].x *= -1.0f;
					vertCoordinate[j].x += 1024;
				}
				if(flipHorizontal) 
				{
					vertCoordinate[j].y *= -1.0f;
					vertCoordinate[j].y += 1024;
				}

				center += vertCoordinate[j];
			}
			center/=_set.piece[i].vertCoord.Length;
			center/=1024;
			spriteList[i]		= Sprite.Create(puzzleImage,_set.dimension,center);
			spriteList[i].name	= "piece " + i;
			spriteList[i].OverrideGeometry( vertCoordinate, _set.piece[i].vertTriangles );
		}
		puzzleCompleted = false;
		
		Utility.ShuffleArray(spriteList);
		gameObject.Audio().clip = clips[0];
		gameObject.Audio().Play();
	}

	public Sprite AnimationBackground
	{
		set 
		{ 
			foreach(SpriteRenderer s in objectAnimationBackground) 
				s.sprite = value; 
		}
	}

	public Sprite AlbumImage
	{
		set { objectAlbumImage.SetSprite(value); }
	}

	public Sprite[] AnimationSpriteSheet
	{
		set { objectAnimation.SpriteSheet = value; }
	}

	public SceneSet.AnimationTransformData TransformData
	{
		set { transformAnimation.TransformData = value; }
	}

	public AudioClip CompletionSound
	{
		set { clips[1] = value; }
	}

	public uint AnimationAudioCueIndex
	{
		set { animationAudioCueIndex = value; }
	}

	public Texture2D PuzzleImage
	{
		set { puzzleImage = value; }
	}

	public Sprite[] SpriteList
	{
		get { return spriteList; }
	}

	public AlbumImage ObjectAlbumImage
	{
		get { return objectAlbumImage; }
	}


	public override void SlideReach()
	{
		base.SlideReach();

		if(puzzleCompleted)		GameManager.Instance.NextPuzzle();
		else 				ObjectPuzzleManager.Instance.DequeueBatch();

	}
}