﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Component Image fader. For fading any images with fadeIn and fadeOut Color
/// </summary>
public class ImageFader : MonoBehaviour
{
	[SerializeField] private Color fadeInColor;
	[SerializeField] private Color fadeOutColor;
	[SerializeField] private Material mat;
	[SerializeField] private string effectName;
	private CountdownTimer timer;
	private SpriteRenderer image;
	private Color startColor;
	private Color endColor;

	private void Start()
	{
		timer = GetComponent<CountdownTimer>();
		image = GetComponent<SpriteRenderer>();
		timer.TimerRunning += (float _elapse) => 
		{
			image.color = Color.Lerp(startColor,endColor,_elapse);
			if(mat != null)
			{
				mat.SetFloat(effectName,1-_elapse);
			}

		};
		timer.TimerEnd += () => 
		{
			image.color = Color.Lerp(startColor,endColor,1);
			if(mat != null)
			{
				mat.SetFloat(effectName,0);
			}
			timer.enabled = false;
			startColor = image.color;
		};
		startColor = image.color;
		timer.enabled = false;
	}

	public void FadeIn()
	{
		endColor = fadeInColor;
		timer.ResetTimer();
		timer.enabled = true;
	}

	public void FadeOut()
	{
		endColor = fadeOutColor;
		timer.ResetTimer();
		timer.enabled = true;
	}

	private void OnDestroy()
	{
		if(mat != null)
		{
			if(mat != null)
			{
				mat.SetFloat(effectName,1);
			}
		}
	}
}
