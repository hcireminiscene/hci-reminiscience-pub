﻿using UnityEngine;
using System.Collections;

public class TranslateComponent : MonoBehaviour 
{
	[SerializeField] private Vector3 startPos;
	[SerializeField] private Vector3 endPos;
	[SerializeField] private bool 	 lookAt;
	[SerializeField] private Space space;

	[SerializeField] private CountdownTimer timer;

	private void Awake()
	{
		
		timer.TimerRunning	+= HandleTimerRunning;
		timer.TimerEnd 		+= HandleTimerEnd;


		timer.enabled = false;
		enabled = false;
	}

	private void HandleTimerEnd()
	{
		if(TranslateEnd != null)
		{
			TranslateEnd();
		}
	}

	private void HandleTimerRunning(float _elapse)
	{
		if(lookAt)
		{
			Vector3 dir = startPos - endPos;
			float angle = Mathf.Atan2(dir.y,dir.x) * Mathf.Rad2Deg;
			transform.localRotation = Quaternion.AngleAxis(angle,Vector3.forward);
		}

		if(space == Space.Self)
		{
			transform.localPosition = Mathfx.Hermite(startPos,endPos,_elapse);
		}
		else
		{
			transform.position = Mathfx.Hermite(startPos,endPos,_elapse);
		}
	}

	public void StartMoving()
	{
		timer.ResetTimer();
		timer.enabled = true;
	}

	public Vector3 StartPos
	{
		set { startPos = value; }
	}
	public Vector3 EndPos
	{
		set { endPos = value; }
	}

	private void OnEnable()
	{
		timer.enabled = true;
	}

	
	private void OnDisable()
	{
		timer.enabled = false;
	}

	public delegate void TranslateEndDelegate();
	public event TranslateEndDelegate TranslateEnd;
}
