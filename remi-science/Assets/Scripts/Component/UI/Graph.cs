﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Graph Component. hardcoded bar graph. (lol)
/// </summary>
public class Graph : MonoBehaviour 
{
	[SerializeField] private GameObject noImage;
	[SerializeField] private Image barImagePref;
	[SerializeField] private Color idleColor;
	[SerializeField] private Color completeColor;


	[SerializeField] private Text[] sectionText;
	[SerializeField] private RawImage[] itemImage;
	[SerializeField] private Image[] barItem1;
	[SerializeField] private Image[] barItem2;
	[SerializeField] private Image[] barItem3;

	private List<int> idleTime0Arr = new List<int>();
	private List<int> idleTime1Arr = new List<int>();
	private List<int> idleTime2Arr = new List<int>();

	private List<int> completeTime0Arr = new List<int>();
	private List<int> completeTime1Arr = new List<int>();
	private List<int> completeTime2Arr = new List<int>();

	private List<GameObject> stuffToDelete = new List<GameObject>();

	private int ob0Puzzle = 0;
	private int ob1Puzzle = 0;
	private int ob2Puzzle = 0;

	private float max_value = 880;
	private float scale_value = 0;
	private float max_height = 880;
	void Start()
	{
		Init();
	}
	public void Init()
	{
		ClearData();

		ob0Puzzle = PlayerPrefs.GetInt("Set_" + GameManager.Instance.SceneSet.name + "_ob0" + "_numPuzzle");
		ob1Puzzle = PlayerPrefs.GetInt("Set_" + GameManager.Instance.SceneSet.name + "_ob1" + "_numPuzzle");
		ob2Puzzle = PlayerPrefs.GetInt("Set_" + GameManager.Instance.SceneSet.name + "_ob2" + "_numPuzzle");
		if(ob0Puzzle == 0 && ob1Puzzle == 0 && ob2Puzzle == 0){
			Debug.Log("Haven't Played this set!");
			noImage.SetActive(true);
		}
		else{
			noImage.SetActive(false);

			int totalTime0 = 0;
			int totalTime1 = 0;
			int totalTime2 = 0;
			for(int i = 0; i < ob0Puzzle; i++)
			{
				idleTime0Arr.Add( PlayerPrefs.GetInt("Set_" + GameManager.Instance.SceneSet.name + "_ob0_piece" + i + "_idlet") );
				completeTime0Arr.Add( PlayerPrefs.GetInt("Set_" + GameManager.Instance.SceneSet.name + "_ob0_piece" + i + "_completet") );

				totalTime0 += idleTime0Arr[i] + completeTime0Arr[i];
			}
			Debug.Log("Set_"+GameManager.Instance.SceneSet.name+ "_ob0_TT" + totalTime0 );

			for(int i = 0; i < ob1Puzzle; i++)
			{
				idleTime1Arr.Add( PlayerPrefs.GetInt("Set_" + GameManager.Instance.SceneSet.name + "_ob1_piece" + i + "_idlet") );
				completeTime1Arr.Add( PlayerPrefs.GetInt("Set_" + GameManager.Instance.SceneSet.name + "_ob1_piece" + i + "_completet") );

				totalTime1 += idleTime1Arr[i] + completeTime1Arr[i];
			}
			Debug.Log("Set_"+GameManager.Instance.SceneSet.name+ "_ob1_TT" + totalTime1 );
			for(int i = 0; i < ob2Puzzle; i++)
			{
				idleTime2Arr.Add( PlayerPrefs.GetInt("Set_" + GameManager.Instance.SceneSet.name + "_ob2_piece" + i + "_idlet") );
				completeTime2Arr.Add( PlayerPrefs.GetInt("Set_" + GameManager.Instance.SceneSet.name + "_ob2_piece" + i + "_completet") );

				totalTime2 += idleTime2Arr[i] + completeTime2Arr[i];
			}
			Debug.Log("Set_"+GameManager.Instance.SceneSet.name+ "_ob2_TT" + totalTime2 );

			// find max value between all 3 total time
			max_value = (totalTime0 > totalTime1) ? totalTime0 : totalTime1;
			max_value = (max_value > totalTime2) ? max_value : totalTime2;
			scale_value = max_height / max_value;

			GenerateData(0, ob0Puzzle,idleTime0Arr,completeTime0Arr);
			GenerateData(1, ob1Puzzle,idleTime1Arr,completeTime1Arr);
			GenerateData(2, ob2Puzzle,idleTime2Arr,completeTime2Arr);

		}

		for(int i = 0; i < 5; i++){
			sectionText[i].text = (max_value/5 * (i+1)).ToString();
		}
		for(int i = 0; i < 3; i++){
			itemImage[i].texture = GameManager.Instance.SceneSet.ObjectPuzzleImage[i];
		}
	}

	private void GenerateData(int _index,int _numPuzzle,List<int> _idleTime, List<int> _completeTime)
	{
		int prevValue = 0;
		for(int i = 0; i < _numPuzzle; i++)
		{
			Image idleBar1 = Instantiate(barImagePref);
			idleBar1.transform.SetParent(itemImage[_index].transform,false);
			idleBar1.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, scale_value * (_idleTime[i] + prevValue) );
			idleBar1.rectTransform.SetSiblingIndex(0);
			idleBar1.color = idleColor;
			prevValue += _idleTime[i];

			Image idleBar2 = Instantiate(barImagePref);
			idleBar2.transform.SetParent(itemImage[_index].transform,false);
			idleBar2.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, scale_value * (_completeTime[i]+prevValue) );
			idleBar2.rectTransform.SetSiblingIndex(0);
			idleBar2.color = completeColor;
			idleBar2.GetComponent<Outline>().effectDistance = new Vector2(8, 8);
			prevValue += _completeTime[i];

			stuffToDelete.Add(idleBar1.gameObject);
			stuffToDelete.Add(idleBar2.gameObject);
		}
		prevValue = 0;
	}

	public void ClearData()
	{
		foreach(GameObject go in stuffToDelete){
			Destroy(go);
		}
		idleTime0Arr.Clear();
		idleTime1Arr.Clear();
		idleTime2Arr.Clear();

		completeTime0Arr.Clear();
		completeTime1Arr.Clear();
		completeTime2Arr.Clear();
	}
}
