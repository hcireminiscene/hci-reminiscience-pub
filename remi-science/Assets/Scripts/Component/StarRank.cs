﻿using UnityEngine;
using System.Collections;

public class StarRank : MonoBehaviour 
{
	[SerializeField] private SpriteRenderer prefStar;
	[SerializeField] private byte	maxNumOfStars;

	[SerializeField] private Vector3 starPos;
	[SerializeField] private Vector3 startScale;
	[SerializeField] private Vector3 endScale;

	private bool			hideStars;
	private byte 			numOfStars;
	private SpriteRenderer[]	starList;
	private WaitTimer		waitTimer;

	private void Start()
	{
		enabled = false;
		starList = new SpriteRenderer[maxNumOfStars];
		for(int i=0;i<maxNumOfStars;i++)
		{
			starList[i]  = Instantiate(prefStar) as SpriteRenderer;
			starList[i].name ="star" + i;
			starList[i].transform.SetParent(transform);
			starList[i].transform.localPosition = Vector3.zero;
			starList[i].gameObject.SetActive(false);
		}
		waitTimer	= GetComponent<WaitTimer>();
		waitTimer.TimerRunning += HandleTimerRunning;
		waitTimer.TimerEnd += HandleTimerEnd;

		waitTimer.ResetTimer();
		waitTimer.enabled = false;

	}


	void HandleTimerRunning (float _elapse)
	{

		for(int i=0;i<numOfStars;i++)
		{
			if(waitTimer.IsWaiting)
			{
				if((1-_elapse) + ((numOfStars-1-i)*0.2f) < 1.0f)
				{
					starList[i].transform.localScale = Mathfx.Hermite(startScale,endScale,(1-_elapse) + ((numOfStars-1-i)*0.2f));
				}
			}
			else
			{
				if(_elapse + ((numOfStars-1-i)*0.2f) < 1.0f)
					starList[i].transform.localScale = Mathfx.Hermite(endScale,startScale,_elapse + ((numOfStars-1-i)*0.2f));
			}
		}
	}

	void HandleTimerEnd ()
	{
		for(int i=0;i<numOfStars;i++)
		{
			if(waitTimer.WaitTime == 0.0f)	
			{
				starList[i].transform.localScale = endScale;
			}
			else		
				starList[i].transform.localScale = startScale;

			starList[i].GetComponent<SpriteRenderer>().color = new Color(1,1,1,1);
		}

		if(GameManager.Instance.GameState != gameState.levelSelect && GameManager.Instance.GameState != gameState.splashScreen)
			ObjectPuzzleManager.Instance.SideOut();
		
		waitTimer.ResetTimer();
		waitTimer.enabled = false;
	}

	public bool HideStars
	{
		set { hideStars = value; }
	}

	public void ShowStars(byte _numOfStars,float _duration,float _waitTime)
	{
		waitTimer.MaxTime = _duration;
		waitTimer.WaitTime = _waitTime;

		ShowStars(_numOfStars);

	}

	public void ShowStars(byte _numOfStars)
	{
		for(int i=0;i<maxNumOfStars;i++)
		{
			starList[i].transform.localScale = Vector2.zero;
		}
		numOfStars = _numOfStars;

		byte center = (byte) (_numOfStars/2.0f);
		for(int i=0;i<_numOfStars;i++)
		{
			starList[i].gameObject.SetActive(true);
			starList[i].transform.localScale = startScale;

			if(hideStars)
				starList[i].GetComponent<SpriteRenderer>().color = new Color(1,1,1,0);

			if(_numOfStars%2==0)
			{
				if((i-center) > center)
					starList[i].transform.localPosition = ((i-center) * starPos);
				else
					starList[i].transform.localPosition = ((i-center+0.5f) * starPos);
			}
			else
			{
				starList[i].transform.localPosition = (i-center) * starPos;
			}
		}
		if(GameManager.Instance.GameState == gameState.objectPuzzle)
		{
			gameObject.Audio().Play();
			EffectManager.Instance.Firework.Play();
		}

		waitTimer.ResetTimer();
		waitTimer.enabled = true;
	}	
}