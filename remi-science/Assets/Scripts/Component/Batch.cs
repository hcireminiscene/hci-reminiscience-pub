﻿using UnityEngine;
using UnityEngine.EventSystems;

using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Batch Component for the Object Puzzle Piece
/// </summary>
public class Batch : MonoBehaviour 
{
	[SerializeField] private List<ClickablePieceBase> puzzleList = new List<ClickablePieceBase>();

	private int completedIndex;
	[SerializeField] private ClickablePieceBase clickedPiece;
	private SlideTo slideTo;

	private void Awake()
	{
		slideTo = GetComponent<SlideTo>();
		slideTo.ReachPosition += Reached;
	}
	

	private void Reached()
	{
		for(int i=0;i<puzzleList.Count;i++)
		{
			puzzleList[i].IsClickable = true;
		}
		GameData.Instance.StartTimer();

		if(GameManager.Instance.GameState == gameState.objectPuzzle)
		{
			FileManager.Instance.StartSessionTimer();
			FileManager.Instance.StartInteractiveTimer();

			if(ObjectPuzzleManager.Instance.NumOfPiece == 3)
				ObjectPuzzleManager.Instance.StartHintTimer(3);
			
			ObjectPuzzleManager.Instance.StartHintTimer();
			GameManager.Instance.CurrentPlayerState = PlayerState.idle;
		}
		else if(GameManager.Instance.GameState == gameState.scenePuzzle)
		{
			ScenePuzzleManager.Instance.StartHintTimer();
			FileManager.Instance.StartInteractiveTimer();
		}
	}

	public PuzzlePieceBase RandomClickablePiece()
	{
		if(clickedPiece == null)
		{
			List<ClickablePieceBase> temp = new List<ClickablePieceBase>();
			for(int i=0;i<puzzleList.Count;i++)
			{
				if(puzzleList[i].IsClickable)
					temp.Add(puzzleList[i]);
			}
			if(temp.Count > 0)
			{
				
				clickedPiece = temp.GetRandom();
				ShiftPuzzlePiecePosition(clickedPiece);
			}
		}
		return clickedPiece;
	}
	public ClickablePieceBase ClickedPiece
	{
		get { return clickedPiece; }
		set { clickedPiece = value; }
	}

	public void SetEndPos(Vector3 _pos)
	{
		slideTo.EndPos = _pos;
	}


	public void SetClickable(bool _value)
	{
		for(int i=0;i<puzzleList.Count;i++)
		{
			puzzleList[i].IsClickable = _value;
		}
	}

	public void StartBatch()
	{
		slideTo.Slide();
		gameObject.Audio().Play();
	}

	public List<ClickablePieceBase> PuzzleList
	{
		get { return puzzleList; }
	}

	public void ShiftPuzzlePiecePosition(PuzzlePieceBase _piece)
	{
		if(_piece.transform.position.z != 0.0f)
		{
			_piece.transform.localPosition = new Vector3(_piece.transform.localPosition.x,
			                                             _piece.transform.localPosition.y,
			                                       		 0.0f);
			for(int i=0;i<puzzleList.Count;i++)
			{
				if(puzzleList[i].IsClickable && puzzleList[i] != _piece)
				{
					for(int j=0;j<puzzleList.Count;j++)
					{
						if(puzzleList[j].IsClickable && puzzleList[j] != _piece && puzzleList[j].transform.position.z > puzzleList[i].transform.position.z)
						{
							puzzleList[i].transform.localPosition = new Vector3(puzzleList[i].transform.localPosition.x,
							                                                    puzzleList[i].transform.localPosition.y,
							                                               		0.1f);
							puzzleList[j].transform.localPosition = new Vector3(puzzleList[j].transform.localPosition.x,
							                                                    puzzleList[j].transform.localPosition.y,
							                                             		0.2f);
							return;
						}
					}
					puzzleList[i].transform.localPosition  = new Vector3(puzzleList[i].transform.localPosition.x,
					                                                     puzzleList[i].transform.localPosition.y,
					                                              		 0.1f);
				}
			}
		}
	}

	public void RemovePiece(ClickablePieceBase _piece)
	{
		puzzleList.Remove(_piece);
	}

	public void PieceSolved(ClickablePieceBase _piece)
	{
		completedIndex++;

		if(GameManager.Instance.GameState == gameState.objectPuzzle)
		{
			GameData.Instance.RecordCompleteData();
			GameData.Instance.StoreData();
		}

		if(clickedPiece != null) 
		{
			if(clickedPiece == _piece)
			{
				clickedPiece.transform.rotation = Quaternion.identity;
			}
			clickedPiece = null;
		}

		if(puzzleList.Count == completedIndex)
		{
			Debug.Log(completedIndex);

			GameData.Instance.StopTimer();

			if(GameManager.Instance.GameState == gameState.objectPuzzle)
			{
				ObjectPuzzleManager.Instance.BatchSolved();
				EffectManager.Instance.CoinBurst(puzzleList);

			}
		}
	}

	public bool Completed
	{
		get { return puzzleList.Count == completedIndex ? true : false; }
	}

	protected virtual void OnDestroy()
	{
		slideTo.ReachPosition	-= Reached;
	}
}
