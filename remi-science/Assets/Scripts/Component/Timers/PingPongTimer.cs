﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Simple Ping-pong Timer
/// </summary>
public class PingPongTimer : TimerBase
{
	private void Start()
	{
		useMaxTime = true;
	}
	private void Update()
	{
		currentTime = Mathf.PingPong(Time.time,maxTime)/maxTime;

		if(TimerRunning != null)
		{
			TimerRunning(currentTime);
		}
	}

	public override void ResetTimer()
	{
		currentTime = 0.0f;
	}

	
	/// <summary>	
	/// Occurs when countdown timer running (attach or detach). 
	/// </summary>
	public override event TimerRunningDelegate TimerRunning;
	///<summary>	
	/// Occurs when countdown timer end (attach or detach).
	/// </summary>
	public override event TimerEndDelegate TimerEnd;
}
