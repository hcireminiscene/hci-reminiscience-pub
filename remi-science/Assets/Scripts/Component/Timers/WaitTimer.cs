﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Simple Wait Timer
/// </summary>
public class WaitTimer : TimerBase
{
	[SerializeField] protected float waitTime;
	[SerializeField] protected bool	 countdownAfterWait;
	protected float currentWaitTime;
	protected bool wait;

	protected void Start()
	{
		useMaxTime = true;
	}
	protected void Update()
	{
		if(!isPause)
		{
			if(!wait)
			{
				if(!countdownAfterWait)
				{
					currentTime = 0.0f;
					if(TimerEnd != null)	
						TimerEnd();
					return;
				}
			}


			if(currentTime > 0.0f && !wait)			currentTime -= Time.deltaTime;
			else if(currentTime < maxTime && wait)		currentTime += Time.deltaTime;

		
			if(currentTime > maxTime && wait)
			{
				if(currentWaitTime < waitTime)	
				{
					currentWaitTime += Time.deltaTime;
					if(WaitTimerRunning!= null)
						WaitTimerRunning((waitTime - currentWaitTime)/waitTime);
				}
				else 							
				{
					wait = false;
					if(WaitTimerEnded!= null)
						WaitTimerEnded();
				}
			}
			else if(currentTime < 0.0f)
			{
				currentTime = 0.0f;
				if(TimerEnd != null)	
					TimerEnd();
			}
			else
			{
				if(TimerRunning != null)		
					TimerRunning(TimeScale);
			}
		}
	}

	public float CurrentWaitTimeScale
	{
		get { return  currentWaitTime; }
	}

	public float WaitTime
	{
		set { waitTime = value; }
		get { return waitTime; }
	}

	public void ResetTimer(float _maxTime)
	{
		maxTime 	= _maxTime;
		ResetTimer();

	}

	public override void ResetTimer()
	{
		wait = true;
		currentWaitTime = currentTime = 0.0f;
	}

	public bool IsWaiting
	{
		get { return wait; }
	}

	public delegate void WaitTimerRunningDelegate(float _elpase);
	public event WaitTimerRunningDelegate WaitTimerRunning;

	public delegate void WaitTimerEndedDelegate();
	public event WaitTimerEndedDelegate WaitTimerEnded;
	
	/// <summary>	
	/// Occurs when countdown timer running (attach or detach). 
	/// </summary>
	public override event TimerRunningDelegate TimerRunning;
	///<summary>	
	/// Occurs when countdown timer end (attach or detach).
	/// </summary>
	public override event TimerEndDelegate TimerEnd;
}
