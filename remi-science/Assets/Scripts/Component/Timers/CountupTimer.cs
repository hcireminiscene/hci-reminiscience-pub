﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Simple Countup Timer
/// </summary>
public class CountupTimer : TimerBase
{
	protected void Update()
	{
		if(!isPause)
		{
			if(!useMaxTime || currentTime < maxTime)
			{
				currentTime += Time.deltaTime;
				if(TimerRunning != null)
					TimerRunning(TimeScale);
			}
			else
			{
				if(TimerEnd != null)
					TimerEnd();
			}
			
		}
	}
	
	public override void ResetTimer()
	{
		currentTime = 0.0f;
	}
	
	private void OnEnable()
	{
		
	}
	
	private void OnDisable()
	{
		
	}
	
	/// <summary>	
	/// Occurs when countdown timer running (attach or detach). 
	/// </summary>
	public override event TimerRunningDelegate TimerRunning;
	///<summary>	
	/// Occurs when countdown timer end (attach or detach).
	/// </summary>
	public override event TimerEndDelegate TimerEnd;
}
