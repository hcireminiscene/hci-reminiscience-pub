﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Simple Countdown Timer
/// </summary>
public class CountdownTimer : TimerBase
{
	protected void Update()
	{
		if(!isPause)
		{
			if(currentTime > 0.0f)
				currentTime -= Time.deltaTime;

			if(currentTime < 0.0f)
			{
				if(TimerEnd != null)	
					TimerEnd();
			}
			else
			{
				if(TimerRunning != null)		
					TimerRunning(TimeScale);
			}
		}
	}

	public override void ResetTimer()
	{
		currentTime = maxTime;
	}

	private void OnEnable()
	{

	}

	private void OnDisable()
	{

	}

	/// <summary>	
	/// Occurs when countdown timer running (attach or detach). 
	/// </summary>
	public override event TimerRunningDelegate TimerRunning;
	///<summary>	
	/// Occurs when countdown timer end (attach or detach).
	/// </summary>
	public override event TimerEndDelegate TimerEnd;

}
