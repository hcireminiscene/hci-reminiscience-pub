﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

/// <summary>
/// Component Money Counter Text, simple component to count up the text
/// </summary>
public class MoneyCounterText : MonoBehaviour 
{
	[SerializeField] private Text 		text;
	[SerializeField] private Vector3 	endScale;
	[SerializeField] private WaitTimer 	timer;

	[SerializeField]	private uint 	startValue;
	[SerializeField]	private uint 	endValue;
	[SerializeField] 	private string 	prefixText;

	private Vector3  	startScale;

	private uint currentValue;

	private void Start()
	{
		text 		= GetComponent<Text>();
		startScale 	= transform.localScale;

		if(timer == null) timer = GetComponent<WaitTimer>();

		timer.TimerRunning 		+= HandleTimerRunning;
		timer.TimerEnd 			+= HandleTimerEnd; 
		timer.WaitTimerRunning 		+= (float _elpase) => 
		{
			currentValue 		= endValue;
			transform.localScale 	= endScale;
			text.text 		= prefixText + currentValue.ToString();
		};
		
		timer.enabled = false;
		text.text = prefixText + currentValue;
	}

	private void HandleTimerRunning(float _elapse)
	{
		if(timer.IsWaiting)
		{
			currentValue 		= (uint)Mathf.Lerp(startValue,endValue,1-_elapse);
			transform.localScale 	= Vector3.Lerp(startScale,endScale,1-_elapse);
		}
		else
		{
			currentValue 		= endValue;
			transform.localScale 	= Vector3.Lerp(endScale,startScale,_elapse);
		}
		text.text = prefixText + currentValue.ToString();
	}

	private void HandleTimerEnd()
	{
		if(!timer.IsWaiting)
		{
			if(GameManager.Instance.GameState == gameState.objectPuzzle)
			{
				ObjectPuzzleManager.Instance.NextBatch();
			}
			timer.ResetTimer();
			timer.enabled = false;
		}
	}
	
	public void UpdateText(uint _value)
	{
		endValue = _value;
		startValue = currentValue;
	}

	public void StartTimer()
	{
		timer.ResetTimer();
		timer.enabled = true;
	}
	private void OnDestroy()
	{
		
		timer.TimerEnd		-= HandleTimerEnd;
		timer.TimerRunning	-= HandleTimerRunning;
	}
}