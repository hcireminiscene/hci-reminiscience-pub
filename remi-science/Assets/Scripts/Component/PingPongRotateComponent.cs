﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Component PingPong, makes GameObject rotate based on startRot and endRot using PingPong effect
/// </summary>
[RequireComponent (typeof(PingPongTimer))]
public class PingPongRotateComponent : MonoBehaviour 
{
	private PingPongTimer timer;
	[SerializeField] private Quaternion startRot;
	[SerializeField] private Quaternion endRot;
	private void Start()
	{
		timer = GetComponent<PingPongTimer>();
		timer.TimerRunning += (float _elapse) => 
		{
			transform.localRotation = Quaternion.Lerp(startRot,endRot,_elapse);
		};
	}

}
