﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

/// <summary>
/// Component SpriteRendererGlow, makes sprite glow!
/// </summary>
public class SpriteRendererGlow : MonoBehaviour 
{
	[SerializeField] private Color startColor;
	[SerializeField] private Color endColor;


	[SerializeField] private UnityEvent OnGlowEnd;
	[SerializeField] private UnityEvent OnGlowMid;

	private WaitTimer		timer;
	private SpriteRenderer	spriteRenderer;

	private void Awake()
	{
		spriteRenderer	 = GetComponent<SpriteRenderer>();
		timer			 = GetComponent<WaitTimer>();

		timer.WaitTimerEnded	+= HandleWaitTimerEnded;
		timer.TimerEnd		+= HandleTimerEnd;
		timer.TimerRunning	+= HandleTimerRunning;

		timer.enabled = false;
	}

	public void StartGlow()
	{
		timer.enabled = true;
	}
	
	public void StartGlow(float _elapse)
	{
		timer.MaxTime = _elapse;
		timer.enabled = true;
	}

	private void HandleTimerRunning(float _elapse)
	{
		if(timer.IsWaiting)
		{
			spriteRenderer.color = Color.Lerp(startColor,endColor,1-_elapse);
		}
		else
		{
			spriteRenderer.color = Color.Lerp(endColor,startColor,_elapse);
		}
	}

	private void HandleTimerEnd()
	{
		if(!timer.IsWaiting)
		{
			spriteRenderer.color = startColor;
			OnGlowEnd.Invoke();
			timer.enabled = false;
		}
		else
		{
			spriteRenderer.color = endColor;
		}
	}

	private void HandleWaitTimerEnded()
	{
		OnGlowMid.Invoke();
	}


}
