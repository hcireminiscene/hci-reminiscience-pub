﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

/// <summary>
/// Component Scale, Scale GameObject
/// </summary>
[RequireComponent (typeof(CountdownTimer))]
public class ScaleComponent : MonoBehaviour 
{
	[SerializeField] private Vector3 startScale;
	[SerializeField] private Vector3 endScale;

	[SerializeField] private UnityEvent action;
	private CountdownTimer timer;

	// Use this for initialization
	void Start () 
	{
		timer = GetComponent<CountdownTimer>();
		timer.TimerRunning += (float _elapse) => 
		{
			transform.localScale = Vector3.LerpUnclamped(startScale,endScale,_elapse);
		};
		timer.TimerEnd += () => 
		{
			if(action !=null)
				action.Invoke();
			timer.enabled = false;
		};
	}
}
