﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Component SlideTo, move GameObject from originPos to endPos
/// </summary>
public class SlideTo : MonoBehaviour
{
	[SerializeField] private Vector3 originPos;
	[SerializeField] private Vector3 endPos;
	[SerializeField] private bool	 isInverse = false;
	[SerializeField] private float	 slideSpeed;
	[SerializeField] private float   checkAmount;
	[SerializeField] private bool	 isSnap;

	protected Vector3 currentPos;

	private void Awake()
	{
		if(originPos == Vector3.zero)
			originPos	= transform.position; 

		currentPos 	= originPos;
		enabled	 	= false;
	}

	private void Update()
	{
		if(enabled)
		{
			if(!isInverse)
			{

				if((transform.localPosition - endPos).sqrMagnitude > checkAmount)
				{
					transform.Translate( (endPos - transform.localPosition) * Time.deltaTime * slideSpeed );
				}
				else
				{
					enabled = false;

					if(isSnap)
						transform.localPosition = endPos;

					if(ReachPosition !=null)	
						ReachPosition();
				}
			}
			else
			{
				if((transform.localPosition - originPos).sqrMagnitude > checkAmount)
				{
					transform.Translate( (originPos - transform.position) * Time.deltaTime * slideSpeed );
				}
				else
				{
					enabled = false;
		
					if(isSnap)
						transform.localPosition = originPos;

					if(ReachPosition !=null)	
						ReachPosition();
				}
			}
		}
	}
	public void Slide()
	{	
		enabled = true;
	}

	public Vector3 OriginPos
	{
		set { originPos = value; }
	}
	public Vector3 EndPos
	{
		set { endPos = value; }
	}

	public bool IsSnap
	{
		get { return isSnap; }
		set { isSnap = value; }
	}

	public bool IsInverse
	{
		get { return isInverse; }
		set { isInverse = value; }
	}

	public delegate void ReachPositionDelegate();
	public event ReachPositionDelegate ReachPosition;
}
