﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Game Data "Manager" well its just another manager.
/// </summary>
public class GameData : Singleton<GameData>
{
	private CountupTimer timer;
	private PieceTiming data;

	private void Start()
	{
		timer = GetComponent<CountupTimer>();
		timer.enabled = false;
	}
	public void ResetData()
	{
		data.index = 0;
	}
	public void StoreData()
	{
		PlayerPrefs.SetInt("Set_"+GameManager.Instance.SceneSet.name+"_ob"+GameManager.Instance.PuzzleIndex+"_piece"+data.index+"_idlet",(int)data.idleTime.TotalMilliseconds);
		PlayerPrefs.SetInt("Set_"+GameManager.Instance.SceneSet.name+"_ob"+GameManager.Instance.PuzzleIndex+"_piece"+data.index+"_completet",(int)data.completeTime.TotalMilliseconds);

		if(GameManager.Instance.GameState == gameState.objectPuzzle)
			FileManager.Instance.SaveText(
							data.index++,
							string.Format("{0:D2}s:{1:D3}ms",data.idleTime.Seconds,data.idleTime.Milliseconds),
							string.Format("{0:D2}s:{1:D3}ms",data.completeTime.Seconds,data.completeTime.Milliseconds),
							ObjectPuzzleManager.Instance.HintsRecieved
			                              );
		else if(GameManager.Instance.GameState == gameState.scenePuzzle)
			FileManager.Instance.SaveText(
				data.index++,
				string.Format("{0:D2}s:{1:D3}ms",data.idleTime.Seconds,data.idleTime.Milliseconds),
				string.Format("{0:D2}s:{1:D3}ms",data.completeTime.Seconds,data.completeTime.Milliseconds),
				ScenePuzzleManager.Instance.HintsRecieved
				);
		
	}

	public void StartTimer()
	{
		if(!timer.enabled)
		{
			timer.ResetTimer();
			timer.enabled = true;
		}
	}

	public void StopTimer()
	{
		timer.ResetTimer();
		timer.enabled = false;
	}

	public void RecordIdleData()
	{
		data.idleTime = System.TimeSpan.FromSeconds(timer.CurrentTime);
		timer.ResetTimer();
	}

	public void RecordCompleteData()
	{
		data.completeTime = System.TimeSpan.FromSeconds(timer.CurrentTime);
		timer.ResetTimer();
	}

	public PieceTiming PieceTiming
	{
		get { return data; }
	}
}