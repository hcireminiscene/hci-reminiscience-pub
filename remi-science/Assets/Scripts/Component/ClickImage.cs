﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;

/// <summary>
/// Hackish way to abuse the EventSystem
/// </summary>
public class ClickImage : MonoBehaviour, IPointerDownHandler
{
	[SerializeField] private UnityEvent action;
	#region IPointerDownHandler implementation
	public void OnPointerDown(PointerEventData _eventData)
	{
		if(action != null)
			action.Invoke();
	}
	#endregion
}
