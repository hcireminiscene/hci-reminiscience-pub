using UnityEngine;
using System.Collections;

/// <summary>
/// Component Path Following, to make GameObject follow a path defined in paths
/// </summary>
public class PathFollowingComponent : MonoBehaviour 
{
	[SerializeField] private Vector3Path[]	paths;

	private Vector3 endPos;
	private Vector3 startPos;
	private float startTime;

	[SerializeField] private int pathChoice;
	private int pathIndex;
	private float stepIndex;

	[SerializeField] private float duration;

	private void Awake()
	{
		startPos = transform.localPosition;
		enabled = false;
		pathIndex = 0;

	}

	private void LateUpdate()
	{
		startTime += Time.deltaTime/(duration*stepIndex);

		if(pathIndex == 0)	transform.localPosition = Vector3.Slerp(startPos, endPos ,startTime);
		else			transform.position 	= Vector3.Slerp(startPos, endPos ,startTime);

		if(  startTime > 1.0f && pathIndex+1 < paths[pathChoice].node.Length )
		{
			startTime = 0.0f;
			pathIndex++;
			startPos = transform.position;
			endPos = paths[pathChoice].node[pathIndex];
		}
		if(startTime >= 1.0f && pathIndex+1 == paths[pathChoice].node.Length)
		{
			// reach pos
			enabled = false;
		}
	}

	public void StarFollow(int _choice)
	{
		pathChoice = _choice;
		enabled = true;
	}

	public void StartFollow()
	{
		enabled = true;
	}
	public void StartFollow(float _duration)
	{
		duration = _duration;
		enabled = true;
	}

	private void OnEnable()
	{
		transform.localPosition = startPos;
		pathIndex = 0;
		
		stepIndex = 1.0f / paths[pathChoice].node.Length;;

		endPos = transform.InverseTransformPoint( paths[pathChoice].node[pathIndex] );
		startTime = 0.0f;
	}

	private void OnDisable()
	{
		transform.localPosition = Vector3.zero;
		startPos = transform.localPosition;
	}

}