﻿using UnityEngine;
using System.Collections;

public class TransformAnimation : MonoBehaviour 
{
	[SerializeField] private SceneSet.AnimationTransformData data;

	private void TimerRunning(float _elapse)
	{
		transform.Lerp(data,_elapse,Space.Self);
	}

	public SceneSet.AnimationTransformData TransformData
	{
		set { data = value; }
	}


	public CountdownTimer.TimerRunningDelegate TimerRunningEvent
	{
		get { return TimerRunning; }
	}
}
