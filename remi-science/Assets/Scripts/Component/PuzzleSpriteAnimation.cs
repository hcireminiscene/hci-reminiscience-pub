﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Component Puzzle Sprite Animation, Sprite animation for all the puzzle pieces
/// </summary>
public class PuzzleSpriteAnimation : SpriteAnimation
{
	[SerializeField] protected AudioSource		source;
	protected uint 	audioCueIndex;

	protected override void Awake()
	{
		base.Awake();
		source 			= gameObject.Audio();
		timer.TimerRunning	+= AudioTimerCue;
		timer.TimerEnd 		+= HandleTimerEnd;
	}

	private void HandleTimerEnd()
	{
		if( GameManager.Instance.GameState == gameState.objectPuzzle)
			ObjectPuzzleManager.Instance.PuzzleAnimationCompleted();

		timer.enabled = false;
	}

	private void AudioTimerCue(float _elapse)
	{
		if(audioCueIndex == animationCurrentIndex && spriteSheet.Length > 1)
		{
			source.Play();
		}
	}

	private void OnDestory()
	{
		timer.TimerRunning -= AudioTimerCue;
	}

	public override void StartAnimation()
	{
		base.StartAnimation();
		if(spriteSheet.Length <= 1) 
		{
			Debug.Log(spriteSheet.Length);
			source.Play();
		}
	}
	
	public void SetAudio(AudioClip _clip,uint _playAtIndex)
	{
		source.clip = _clip;
		audioCueIndex = _playAtIndex;
		
		if(_clip.length > 3.0f)
			timer.MaxTime = _clip.length;
		else
			timer.MaxTime = 3.0f;
	}

}
