using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

/// <summary>
/// Fake Album UI that you see in Level Selection
/// </summary>
public class Album : MonoBehaviour,IPointerUpHandler,IPointerDownHandler,IDragHandler
{
	[SerializeField] private BoxCollider2D[]	arrowColliderList;
	[SerializeField] private SpriteRenderer[] 	objectPuzzleRendererList;
	[SerializeField] private SpriteRenderer 	scenePuzzleRenderer;
	[SerializeField] private SelectArrow		selectArrow;

	[SerializeField] private Transform			pageTransform;
	[SerializeField] private StarRank[]			flippedPageStars;
	[SerializeField] private SpriteRenderer[] 	pageObjectPuzzleRendererList;
	[SerializeField] private SpriteRenderer 	pageScenePuzzleRenderer;
	[SerializeField] private GameObject		coverPage;

	[SerializeField] private Quaternion			startRot;
	[SerializeField] private Quaternion			endRot;

	[SerializeField] private float swipeDistance;
	[SerializeField] private BoxCollider2D 		objectPuzzleCollider;
	[SerializeField] private StarRank[]			albumStars;

	[SerializeField] private AudioClip[]		pageTurn;
	[SerializeField] private AudioClip			coverTurn;

	private Vector2 clickPos;
	private CountupTimer	waitTimer;
	private PingPongTimer 	blinkTimer;
	private CountdownTimer 	flipTimer;
	[SerializeField]
	private bool isClickable;
	private void Awake()
	{
		if(Camera.main.aspect >= 1.7)
		{
			transform.Translate(-1.0f, 0.0f, 0.0f);
		}
	}
	private void Start()
	{
		blinkTimer = GetComponent<PingPongTimer>();


		if(!GameManager.Instance.SceneSet.Completed)
		{
			blinkTimer.TimerRunning += HandleBlinkTimerRunning;

			//arrowColliderList[0].gameObject.SetActive(false);
			//arrowColliderList[1].gameObject.SetActive(false);
		}
		else
		{
			for(int i=0;i<objectPuzzleRendererList.Length;i++)
			{
				objectPuzzleRendererList[i].material.SetFloat("_EffectAmount",0.0f);
				albumStars[i].ShowStars(GameManager.Instance.SceneSet.StarAward[i],2.0f,2.0f);
			}
			scenePuzzleRenderer.material.SetFloat("_EffectAmount",0.0f);
		}

		flipTimer = GetComponent<CountdownTimer>();
		flipTimer.enabled = false;
		flipTimer.TimerEnd += HandleFlipTimerEnd;

		waitTimer = GetComponent<CountupTimer>();
		waitTimer.enabled = false;

		waitTimer.TimerEnd += HandleWaitTimerEnd;

		isClickable = false;
		pageTransform.gameObject.SetActive(false);
		SetCurrentPage(true);
		
	}

	public bool IsClickable
	{
		set { isClickable = value; }
	}

	private void HandleWaitTimerEnd()
	{
		NextPage();
		UIManager.Instance.ShowButtons();
		isClickable = true;
		waitTimer.enabled = false;
		waitTimer.ResetTimer();
	}

	public void StartAlbumSequence()
	{
		isClickable = false;
		for(int i=0;i<objectPuzzleRendererList.Length;i++)
		{
			objectPuzzleRendererList[i].material.SetFloat("_EffectAmount",0.0f);
			albumStars[i].ShowStars(GameManager.Instance.SceneSet.StarAward[i],2.0f,2.0f);
		}
		scenePuzzleRenderer.material.SetFloat("_EffectAmount",0.0f);
		blinkTimer.TimerRunning -= HandleBlinkTimerRunning;

		waitTimer.enabled = true;
	}

	public void Completed()
	{
		isClickable = false;
		for(int i=0;i<objectPuzzleRendererList.Length;i++)
		{
			objectPuzzleRendererList[i].material.SetFloat("_EffectAmount",0.0f);
		}
		
		blinkTimer.TimerRunning -= HandleBlinkTimerRunning;
	}


	
	private void HandleFlipTimerRunningCoverPage (float _elapse)
	{
		coverPage.transform.localRotation = Quaternion.Lerp(startRot,endRot,_elapse);
	}

	private void HandleFlipTimerRunningNext (float _elpase)
	{
		pageTransform.localRotation = Quaternion.Lerp(startRot,endRot,_elpase);
	}
	private void HandleFlipTimerRunningPrevious (float _elpase)
	{
		pageTransform.localRotation = Quaternion.Lerp(endRot,startRot,_elpase);
	}
	private void HandleBlinkTimerRunning (float _elapse)
	{
		objectPuzzleRendererList[0].material.SetFloat("_EffectAmount",_elapse);
	}

	private void HandleFlipTimerEnd()
	{
		if(GameManager.Instance.GameState == gameState.splashScreen)
		{
			GameManager.Instance.GameState = gameState.levelSelect;
			flipTimer.TimerRunning -= HandleFlipTimerRunningCoverPage;
			Destroy( coverPage );
			return;
		}

		if(GameManager.Instance.GameState != gameState.levelSelect) return;


		isClickable = true;
		flipTimer.enabled = false;
		flipTimer.ResetTimer();


		pageTransform.localRotation = Quaternion.identity;
		
		SetCurrentPage(true);
		ResetCurrentPage();
		scenePuzzleRenderer.sprite		= GameManager.Instance.SceneSet.ScenePuzzleImage;
		pageScenePuzzleRenderer.sprite	= GameManager.Instance.SceneSet.ScenePuzzleImage;

		pageScenePuzzleRenderer.material.SetFloat("_EffectAmount",1.0f);
		
		for(int i=0;i<objectPuzzleRendererList.Length;i++)
		{
			pageObjectPuzzleRendererList[i].sprite = Sprite.Create( GameManager.Instance.SceneSet.ObjectPuzzleImage[i],new Rect(0,0,1024,1024), Vector2.one/2);
			pageObjectPuzzleRendererList[i].material.SetFloat("_EffectAmount",1.0f);
		}
		
		if(!GameManager.Instance.SceneSet.Completed)
		{
			blinkTimer.TimerRunning += HandleBlinkTimerRunning;
		}	
		else
		{
			for(int i=0;i<objectPuzzleRendererList.Length;i++)
			{
				objectPuzzleRendererList[i].material.SetFloat("_EffectAmount",0.0f);
				albumStars[i].HideStars = false;
			}
			scenePuzzleRenderer.material.SetFloat("_EffectAmount",0.0f);
		}

		pageTransform.gameObject.SetActive(false);
	}

	#region IDragHandler implementation

	public void OnDrag (PointerEventData _eventData)
	{
		Vector2 deltaPos = _eventData.position - clickPos;
	}

	#endregion

	#region IPointerDownHandler implementation

	public void OnPointerDown (PointerEventData _eventData)
	{
		clickPos = _eventData.position;
	}

	#endregion

	#region IPointerUpHandler implementation

	public void OnPointerUp (PointerEventData _eventData)
	{
		if(!isClickable) return;

		if(GameManager.Instance.GameState == gameState.splashScreen)
		{
			//flip cover page
			gameObject.Audio().clip = coverTurn;
			gameObject.Audio().Play();
			flipTimer.TimerRunning += HandleFlipTimerRunningCoverPage;
			flipTimer.enabled = true;

			isClickable = false;
			return;
		}


		if(objectPuzzleCollider.OverlapPoint(_eventData.pointerCurrentRaycast.worldPosition) && 
		   objectPuzzleCollider.OverlapPoint(_eventData.pointerPressRaycast.worldPosition))
		{
			blinkTimer.TimerRunning -= HandleBlinkTimerRunning;
			GameManager.Instance.GameState = gameState.objectPuzzle;
			
			FileManager.Instance.CreateFile();
			GameManager.Instance.CreatePuzzle();
			
			SoundManager.Instance.FadeBgm(gameState.objectPuzzle);
			arrowColliderList[0].gameObject.SetActive(false);
			arrowColliderList[1].gameObject.SetActive(false);

			isClickable = false;
		}

		if(arrowColliderList[0].isActiveAndEnabled)
		{
			Vector2 deltaPos = _eventData.position - _eventData.pressPosition;
			
			if(Mathf.Abs(deltaPos.x) > swipeDistance)
			{
				if(deltaPos.x > 0.0f)		PreviousPage();
				else				NextPage();
			}

			if(arrowColliderList[0].OverlapPoint(_eventData.pointerCurrentRaycast.worldPosition))			NextPage();
			else if(arrowColliderList[1].OverlapPoint(_eventData.pointerCurrentRaycast.worldPosition))		PreviousPage();
		}

		Debug.Log(isClickable);
	}

	#endregion


	protected void OnDestroy()
	{
		// clearing events

		flipTimer.TimerRunning	-= HandleFlipTimerRunningNext;
		flipTimer.TimerRunning	-= HandleFlipTimerRunningPrevious;
		flipTimer.TimerEnd		-= HandleFlipTimerEnd;

		blinkTimer.TimerRunning	-= HandleBlinkTimerRunning;

	}

	private void SetCurrentPage(bool _nextPage)
	{
		if(_nextPage)
		{
			scenePuzzleRenderer.sprite = GameManager.Instance.SceneSet.ScenePuzzleImage;
			for(int i=0;i<objectPuzzleRendererList.Length;i++)
			{
				objectPuzzleRendererList[i].sprite = Sprite.Create( GameManager.Instance.SceneSet.ObjectPuzzleImage[i], new Rect(0,0,1024,1024),Vector2.one/2);
			}
		}
		else
		{
			for(int i=0;i<objectPuzzleRendererList.Length;i++)
			{
				albumStars[i].ShowStars(GameManager.Instance.SceneSet.StarAward[i]);
			}
		}


	}

	private void NextPage()
	{
		Debug.Log("Next");

		gameObject.Audio().clip = pageTurn[Random.Range(0,pageTurn.Length)];
		gameObject.Audio().Play();
		pageTransform.gameObject.SetActive(true);
		// left
		
		for(int i=0;i<albumStars.Length;i++)
		{
			flippedPageStars[i].ShowStars(GameManager.Instance.SceneSet.StarAward[i],0.5f,0.0f);
		}

		GameManager.Instance.NextSet();

		for(int i=0;i<albumStars.Length;i++)
		{
			albumStars[i].ShowStars(GameManager.Instance.SceneSet.StarAward[i],0.5f,0.0f);
		}

		flipTimer.TimerRunning -= HandleFlipTimerRunningNext;
		flipTimer.TimerRunning -= HandleFlipTimerRunningPrevious;
		flipTimer.TimerRunning += HandleFlipTimerRunningNext;
		flipTimer.enabled = true;
		flipTimer.ResetTimer();
		pageTransform.gameObject.SetActive(true);
		
		SetFlipPage(scenePuzzleRenderer,objectPuzzleRendererList);
		SetCurrentPage(true);
		ResetCurrentPage();
		
		if(!GameManager.Instance.SceneSet.Completed)
		{
			blinkTimer.TimerRunning += HandleBlinkTimerRunning;
			if(!arrowColliderList[0].gameObject.activeSelf)
			{
				arrowColliderList[0].gameObject.SetActive(true);
				arrowColliderList[1].gameObject.SetActive(true);
			}
		}
		else
		{
			if(!arrowColliderList[0].gameObject.activeSelf)
			{
				arrowColliderList[0].gameObject.SetActive(true);
				arrowColliderList[1].gameObject.SetActive(true);
			}
			blinkTimer.TimerRunning -= HandleBlinkTimerRunning;
			for(int i=0;i<objectPuzzleRendererList.Length;i++)
			{
				objectPuzzleRendererList[i].material.SetFloat("_EffectAmount",0.0f);
			}
			scenePuzzleRenderer.material.SetFloat("_EffectAmount",0.0f);
		}
	}

	private void PreviousPage()
	{
		

		gameObject.Audio().clip = pageTurn[Random.Range(0,pageTurn.Length)];
		gameObject.Audio().Play();
		pageTransform.gameObject.SetActive(true);
		// right
		GameManager.Instance.PreviousSet();
		SetCurrentPage(false);

	

		for(int i=0;i<flippedPageStars.Length;i++)
		{
			albumStars[i].HideStars = true;
			albumStars[i].ShowStars(GameManager.Instance.SceneSet.StarAward[i],0.49f,0.0f);
			flippedPageStars[i].ShowStars(GameManager.Instance.SceneSet.StarAward[i],0.5f,0.0f);
		}

		flipTimer.TimerRunning -= HandleFlipTimerRunningNext;
		flipTimer.TimerRunning -= HandleFlipTimerRunningPrevious;
		flipTimer.TimerRunning += HandleFlipTimerRunningPrevious;
		
		flipTimer.enabled = true;
		flipTimer.ResetTimer();
		pageTransform.localRotation = endRot;
		pageTransform.gameObject.SetActive(true); 	
		
//		ResetCurrentPage();
		
		if(!GameManager.Instance.SceneSet.Completed)
		{
			blinkTimer.TimerRunning += HandleBlinkTimerRunning;
		}	
		else
		{
			blinkTimer.TimerRunning -= HandleBlinkTimerRunning;
			for(int i=0;i<pageObjectPuzzleRendererList.Length;i++)
			{
				pageObjectPuzzleRendererList[i].material.SetFloat("_EffectAmount",0.0f);
			}
			pageScenePuzzleRenderer.material.SetFloat("_EffectAmount",0.0f);
		}
		
		SetFlipPage(GameManager.Instance.SceneSet.ScenePuzzleImage,GameManager.Instance.SceneSet.ObjectPuzzleImage);
	}

	private void SetFlipPage(Sprite _sceneRenderer, Texture2D[] _objectPuzzleTexture)
	{
		pageScenePuzzleRenderer.sprite = _sceneRenderer;
		
		for(int i=0;i<pageObjectPuzzleRendererList.Length;i++)
		{
			pageObjectPuzzleRendererList[i].sprite = Sprite.Create( _objectPuzzleTexture[i], new Rect(0,0,1024,1024),Vector2.one/2);
		}
	}

	private void SetFlipPage(SpriteRenderer _sceneRenderer, SpriteRenderer[] _objectReneder)
	{
		pageScenePuzzleRenderer.sprite = _sceneRenderer.sprite;
		pageScenePuzzleRenderer.material.SetFloat("_EffectAmount",_sceneRenderer.material.GetFloat("_EffectAmount"));
		
		for(int i=0;i<pageObjectPuzzleRendererList.Length;i++)
		{
			pageObjectPuzzleRendererList[i].sprite = _objectReneder[i].sprite;
			pageObjectPuzzleRendererList[i].material.SetFloat("_EffectAmount",_objectReneder[i].material.GetFloat("_EffectAmount"));
		}
	}

	private void ResetCurrentPage()
	{
		scenePuzzleRenderer.material.SetFloat("_EffectAmount",1.0f);
		for(int i=0;i<objectPuzzleRendererList.Length;i++)
		{
			objectPuzzleRendererList[i].material.SetFloat("_EffectAmount",1.0f);
		}
	}
}
