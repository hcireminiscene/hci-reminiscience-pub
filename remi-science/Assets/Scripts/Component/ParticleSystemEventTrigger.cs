﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

/// <summary>
/// Component for ParticleSystem. Going to abuse it yet again so that we can play some sounds or anything when particle OnBirth/OnDeath/OnCollision is triggered from Unity ParticleSystem
/// </summary>
public class ParticleSystemEventTrigger : MonoBehaviour
{
	[SerializeField,EnumFlagAttribute]	private ParticleSubEmitterType currentType;
	[SerializeField] private UnityEvent triggeredEvent;

	private ParticleSystem	particleSystemComponent;
	private int 			particlesCount;

	// Use this for initialization
	private void Start()
	{
		particleSystemComponent = GetComponent<ParticleSystem>();
		particlesCount = particleSystemComponent.particleCount;

		switch(currentType)
		{
			case ParticleSubEmitterType.OnBirth:			OnUpdateEvent += HandleOnUpdateOnBirthEvent;		break;
			case ParticleSubEmitterType.OnDeath:			OnUpdateEvent += HandleOnUpdateOnDeathEvent;		break;
			case ParticleSubEmitterType.OnCollision:		OnUpdateEvent += HandleOnUpdateOnCollisionEvent;	break;
		}
	}

	private void HandleOnUpdateOnBirthEvent()
	{
		if(particlesCount < particleSystemComponent.particleCount)
		{
			triggeredEvent.Invoke();
		}
		particlesCount = particleSystemComponent.particleCount;
	}
	private void HandleOnUpdateOnDeathEvent()
	{
		if(particlesCount > particleSystemComponent.particleCount)
		{
			triggeredEvent.Invoke();
		}
		particlesCount = particleSystemComponent.particleCount;
	}
	private void HandleOnUpdateOnCollisionEvent()
	{
		// not implemented
	}
	
	// Update is called once per frame
	private void LateUpdate()
	{
		if(OnUpdateEvent != null)
		{
			OnUpdateEvent();
		}
		particlesCount = particleSystemComponent.particleCount;
	}

	protected void OnDestory()
	{
		OnUpdateEvent -= HandleOnUpdateOnBirthEvent;
		OnUpdateEvent -= HandleOnUpdateOnDeathEvent;
		OnUpdateEvent -= HandleOnUpdateOnCollisionEvent;
	}

	private delegate void OnUpdateDelegate();
	private event OnUpdateDelegate OnUpdateEvent;
		       
}
