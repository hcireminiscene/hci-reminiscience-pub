﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

/// <summary>
/// Custom touch module. Another Hackish way to add wait when player clicked on objects
/// </summary>
public class CustomTouchInputModule : TouchInputModule
{
	public override void Process()
	{
		base.Process();
	}

	protected override void ProcessDrag(PointerEventData _pointerEventData)
	{
	
		if(_pointerEventData.delta.magnitude < eventSystem.pixelDragThreshold)
		{
			ExecuteEvents.Execute(_pointerEventData.pointerPress,_pointerEventData,ExecuteEventHelper.OnWaitHandler);
		}

		base.ProcessDrag(_pointerEventData);

	}

	public void RemovePointerData(PointerEventData _pointerEventData)
	{
		base.RemovePointerData(_pointerEventData);
	}
}