﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Scene Puzzle Piece, 1 slice of the Scene Puzzle that will be colored
/// </summary>
public class ScenePiece : PuzzlePieceBase 
{
	private ScenePlayingPiece playingPiece;

	private float oldValue;
	private float currentValue;
	private CountdownTimer timer;

	public ScenePlayingPiece PlayPiece
	{
		set { playingPiece = value; }
		get { return playingPiece; }
	}

	protected void Start()
	{
		image = GetComponent<SpriteRenderer>();

		currentValue = 1.0f;
		timer = GetComponent<CountdownTimer>();
		timer.TimerRunning 	+= (float _elapse) => 	{	image.material.SetFloat("_EffectAmount",Mathf.Lerp(oldValue,currentValue,_elapse));		};
		timer.TimerEnd 		+= () => 				
		{	
			timer.enabled = false;	
			//ScenePuzzleManager.Instance.CurrentBatch.RemovePiece(playingPiece);
			ScenePuzzleManager.Instance.BatchSolved();
		};
		timer.enabled = false;

	}

	public void CompletePiece(float _value)
	{
		oldValue = currentValue;
		currentValue = _value;
		timer.enabled = true;
		timer.ResetTimer();
	}
}