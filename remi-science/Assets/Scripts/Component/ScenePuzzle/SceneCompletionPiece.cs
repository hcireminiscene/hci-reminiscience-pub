﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Scene Completion Puzzle Piece, location where Scene Puzzle Piece will be placed into
/// </summary>
public class SceneCompletionPiece : PuzzlePieceBase
{
	[SerializeField] private Vector3 scale;
	[SerializeField] private Quaternion rotation;

	public void SetRotation(Quaternion _rotation)
	{
		rotation = _rotation;
		transform.rotation = _rotation;
	}

	public void SetScale(Vector3 _scale)
	{
		scale = _scale;
		transform.localScale = _scale;
	}
}