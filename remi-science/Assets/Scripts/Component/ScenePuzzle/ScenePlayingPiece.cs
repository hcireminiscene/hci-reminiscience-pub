﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

/// <summary>
/// Scene Puzzle Piece, 1 clickable Scene Puzzle Piece for player to complete
/// </summary>
public class ScenePlayingPiece : ClickablePieceBase
{
	[SerializeField] private float maxTime;
	private float currentTime = 0.0f;

	private Quaternion startRotation;
	private Quaternion endRotation;
	private Vector3 endScale;
	private Vector3 startScale;

	public void SetEndRotation(Quaternion _rotation)
	{
		endRotation 	= _rotation;
		startRotation 	= transform.rotation;
	}

	public void SetEndScale(Vector3 _scale)
	{
		endScale 	= _scale;
		startScale	= transform.localScale;
	}

	#region implemented abstract members of ClickablePieceBase
	protected override void Complete ()
	{
		transform.rotation = Quaternion.Euler(Vector3.zero);	
		isClickable = false;
		StartCoroutine("MoveToPlace");

		
		GameData.Instance.RecordCompleteData();
		GameData.Instance.StoreData();

		ScenePuzzleManager.Instance.PauseHints();
		ScenePuzzleManager.Instance.ResetHint();
	}

	#endregion

	#region IPointerDownHandler implementation

	public override void OnPointerDown (PointerEventData _eventData)
	{
		base.OnPointerDown(_eventData);

		//ScenePuzzleManager.Instance.ResetHint(2);
		
		ScenePuzzleManager.Instance.CurrentBatch.ClickedPiece = this;
		ScenePuzzleManager.Instance.CurrentBatch.ShiftPuzzlePiecePosition(this);
		
		ScenePuzzleManager.Instance.PauseHints();
	}

	#endregion

	#region IPointerUpHandler implementation

	public override void OnPointerUp (PointerEventData _eventData)
	{
		base.OnPointerUp(_eventData);
		transform.rotation 	= Quaternion.Euler(Vector3.zero);

		ScenePuzzleManager.Instance.ResumeHints();
		
		//ObjectPuzzleManager.Instance.CurrentBatch.ClickedPiece = null;
		//not main timer
		ScenePuzzleManager.Instance.ResetIdleHintTimer();
	}

	#endregion

	#region IBeginDragHandler implementation

	public override void OnBeginDrag (PointerEventData _eventData)
	{
		base.OnBeginDrag(_eventData);

		ScenePuzzleManager.Instance.PauseHints();
		//ScenePuzzleManager.Instance.ResetHint(2);
	}

	#endregion

	#region IDragHandler implementation
	public override void OnDrag (PointerEventData _eventData)
	{
		base.OnDrag(_eventData);
	}
	#endregion

	
	protected IEnumerator MoveToPlace()
	{
		while(true)
		{
			transform.localScale = Vector3.Lerp(startScale,endScale,(currentTime+=Time.deltaTime)/maxTime);
			if(endRotation != Quaternion.identity || endRotation != transform.rotation.Zero())
				transform.rotation	 = Quaternion.Lerp(startRotation,endRotation,(currentTime+=Time.deltaTime)/maxTime);

			if(currentTime > maxTime) 
			{
				currentTime = 0.0f;
				StopCoroutine("MoveToPlace");
				
				ScenePuzzleManager.Instance.ResetHint();
				ScenePuzzleManager.Instance.CompletePiece(this);
			}
			yield return null;
		}
		
	}
}
