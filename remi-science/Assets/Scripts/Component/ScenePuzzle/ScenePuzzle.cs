﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Scene Puzzle, The scene puzzle component when all 3 object puzzle have been completed.
/// </summary>
public class ScenePuzzle : PuzzleBase 
{
	[SerializeField] private Sprite			scenePuzzleSprite;
	[SerializeField] private Sprite[]		scenePuzzlePieceSpriteList;
	[SerializeField] private Vector3 startPos;
	[SerializeField] private Vector3 endPos;

	[SerializeField] private Vector3 startScale;
	[SerializeField] private Vector3 endScale;

	private int animationCountIndex;
	private CountdownTimer countdownTimer;
	[SerializeField] private GameObject[]						scenePuzzleAnimationOffset;
	[SerializeField] private PuzzleSpriteAnimation[]			scenePuzzleAnimation;
	[SerializeField] private TransformAnimation[]				scenePuzzleTransformAnimation;


	private void Start()
	{
		if(scenePuzzleAnimation == null)
		{
			scenePuzzleAnimation = new PuzzleSpriteAnimation[3];
			scenePuzzleAnimation = GetComponentsInChildren<PuzzleSpriteAnimation>();
		}

		
		if(Camera.main.aspect < 1.4f) endPos = new Vector3(1.45f,0.8f,1.0f);

		countdownTimer = GetComponent<CountdownTimer>();
		countdownTimer.TimerRunning += ScaleTimerRunning;
		countdownTimer.TimerEnd		+= ScaleCompleted;
		countdownTimer.enabled = false;
		animationCountIndex = 0;

		// needs to change?
		for(int i=0;i<scenePuzzleAnimation.Length;i++)
		{
			scenePuzzleAnimation[i].AnimationEnd += HandleAnimationEnd;
			scenePuzzleAnimation[i].Timer.TimerRunning += scenePuzzleTransformAnimation[i].TimerRunningEvent;
		}
	}


	private	void ScaleTimerRunning(float _elapse)
	{
		transform.localPosition = Mathfx.Hermite(startPos,endPos,_elapse);
		transform.localScale = Mathfx.Hermite(startScale,endScale,_elapse); 
	}

	
	private void ScaleCompleted()
	{
		countdownTimer.enabled = false;

		for(int i=0;i<scenePuzzleAnimation.Length;i++)
		{
			scenePuzzleAnimation[i].gameObject.SetActive(true);
			if(GameManager.Instance.SceneSet.ScenePuzzlePlayAnimation[i])
				scenePuzzleAnimation[i].StartAnimation();
			else
				animationCountIndex++;

		}
		
	}

	private void HandleAnimationEnd()
	{
		// do something.
		animationCountIndex++;
		if(animationCountIndex == scenePuzzleAnimation.Length)
		{
			UIManager.Instance.SlideIn(1);
			EffectManager.Instance.DropFullscreenCoins();
		}
	}

	public TransformAnimation[] ScenePuzzleTransformAnimation
	{
		get { return scenePuzzleTransformAnimation; }
	}

	
	public GameObject[]	ScenePuzzleAnimationOffset
	{
		get { return scenePuzzleAnimationOffset; }
	}

	public PuzzleSpriteAnimation[]	ScenePuzzleAnimation
	{
		get { return scenePuzzleAnimation; }
	}
	
	public Sprite ScenePuzzleSprite
	{
		set { scenePuzzleSprite = value; }
		get { return scenePuzzleSprite; }
	}

	public Sprite[] ScenePuzzlePieceSpriteList
	{
		set { scenePuzzlePieceSpriteList = value; }
		get { return scenePuzzlePieceSpriteList; }
	}

	public void PlayAudio()
	{	
		gameObject.Audio().Play();
	}

	public void Complete()
	{
		
		FileManager.Instance.EndInteractiveTimer();
		FileManager.Instance.EndSessionTimer();

		countdownTimer.enabled = true;
		UIManager.Instance.SlideIn(0);
	}

	public override void SlideReach()
	{
		base.SlideReach();

		if(!slideTo.IsInverse)	
			ScenePuzzleManager.Instance.CurrentBatch.StartBatch();
		else															
		{
			Destroy(this.gameObject);		
		}
	}

	protected void OnDestroy()
	{
		countdownTimer.TimerRunning -= ScaleTimerRunning;
		countdownTimer.TimerEnd		-= ScaleCompleted;

	}
}