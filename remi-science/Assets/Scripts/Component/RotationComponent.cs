﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Component Rotation, to rotate GameObject
/// </summary>
public class RotationComponent : MonoBehaviour 
{
	[SerializeField] private int maxNumOfRotation;
	[SerializeField] protected Vector3 amountToRotate;


	private int numOfRotation;

	private void Update()
	{
		if(transform.localEulerAngles.y < amountToRotate.y)
		{
			numOfRotation++;
		}
		if(numOfRotation <= maxNumOfRotation)
			transform.Rotate(amountToRotate);
	}
}
