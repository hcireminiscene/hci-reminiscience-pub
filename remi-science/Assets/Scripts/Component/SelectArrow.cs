﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Component Select Arrow, for the fake album arrow indicator
/// </summary>
public class SelectArrow : MonoBehaviour
{
	[SerializeField] private Vector3 deltaPos;
	private PingPongTimer timer;
	private Vector3 endPos;
	private Vector3 startPos;

	private void Start()
	{
		timer = GetComponent<PingPongTimer>();
		timer.TimerRunning += HandleTimerRunning;
		startPos = transform.position;
		endPos   = startPos + deltaPos;
	}

	void HandleTimerRunning (float _elapse)
	{
		transform.position = Vector3.Lerp(startPos,endPos,_elapse);
	}
}
