﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Fake Album Image for the Level Select
/// </summary>
public class AlbumImage : MonoBehaviour 
{
 	[SerializeField] private SpriteRenderer 	spriteRenderer;
	private WaitTimer				waitTimer;

	[SerializeField] private Vector3 startPos;
	[SerializeField] private Vector3 endPos;

	private void Awake()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		waitTimer = GetComponent<WaitTimer>();

		waitTimer.enabled	=  enabled = false;
		spriteRenderer.enabled 	= false;
		waitTimer.TimerRunning  += (_elapse) => 		{	transform.localPosition = Mathfx.Hermite(startPos,endPos,_elapse);	};
		waitTimer.TimerEnd	+= ()	=> 			{ 	spriteRenderer.enabled = false; 					};
		waitTimer.WaitTimerRunning += (float _elpase)	=> 	{	spriteRenderer.material.SetFloat("_EffectAmount",_elpase*0.6f);		};
	}


	public void SetSprite(Sprite _sprite)
	{
		spriteRenderer.sprite = _sprite;
	}


	public void SlideIn()
	{
		waitTimer.enabled = true;
		spriteRenderer.enabled = true;
		spriteRenderer.material.SetFloat("_EffectAmount",1.0f);
	}
}