﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Component DottedLineRenderer. For HintArrow 
/// </summary>
public class DottedLineRenderer : MonoBehaviour 
{
	[SerializeField] private Vector3[] positions;
	[SerializeField, Tooltip("X for scale")] private Vector2 	tiling;
	[SerializeField] private Material mat;
	private LineRenderer line;
	// Use this for initialization
	void Awake() 
	{
		line = GetComponent<LineRenderer>();
		line.material	=  mat;
		line.SetVertexCount(positions.Length);
	}
	
	// Update is called once per frame
	void FixedUpdate() 
	{
		for(int i=0;i<positions.Length;i++)
		{
			line.SetPosition(i,positions[i]);
		}
		line.material.mainTextureScale = new Vector2( (positions[0]-positions[1]).magnitude*tiling.x ,tiling.y);
	}

	public Vector3[] Positions
	{
		get { return positions; }
	}

	private void OnEnable()
	{
		for(int i=0;i<positions.Length;i++)
		{
			line.SetPosition(i,positions[i]);
		}
		line.enabled = true;
	}
	private void OnDisable()
	{
		line.enabled = false;
	}
}
