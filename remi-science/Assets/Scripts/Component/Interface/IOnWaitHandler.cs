﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

/// <summary>
/// Own Unity EventSystem for Wait Handling
/// </summary>
public interface IOnWaitHandler : IEventSystemHandler
{
	void OnWait(PointerEventData _eventData);
}
