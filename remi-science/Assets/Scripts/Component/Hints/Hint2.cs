﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Component for hint 2 (Wiggle)
/// </summary>
public class Hint2 : HintBase
{
	[SerializeField] private Transform puzzlePieceTransform;

	private PingPongTimer timer;
	private float rotationValue;

	private void Start()
	{
		timer = GetComponent<PingPongTimer>();
		timer.TimerRunning += HandleTimerRunning;

		timer.enabled = false;
	}

	public override void StartHint(ClickablePieceBase _puzzlePiece)
	{
		base.StartHint(_puzzlePiece);
		puzzlePieceTransform	 = _puzzlePiece.gameObject.transform;

		timer.enabled = true;
	}

	public override void PauseHint()
	{
		base.PauseHint();

		puzzlePieceTransform.rotation = Quaternion.identity;
	}

	public override void ClearHint()
	{
		base.ClearHint();

		if(puzzlePieceTransform 	!= null)	
			puzzlePieceTransform.rotation	
				= Quaternion.Euler(new Vector3(0.0f,0.0f,rotationValue));


		puzzlePieceTransform	 = null;
	}

	protected void OnDestroy()
	{
		timer.TimerRunning -= HandleTimerRunning;
	}
	
	
	private void HandleTimerRunning(float _elpase)
	{
		if(!started)
		{
			return;
		}

		switch(GameManager.Instance.GameState)
		{
			case gameState.objectPuzzle:
				if(ObjectPuzzleManager.Instance.CurrentBatch.ClickedPiece != null)
				{
					if(ObjectPuzzleManager.Instance.CurrentBatch.ClickedPiece.transform != puzzlePieceTransform)
					{
						
						puzzlePieceTransform.rotation 		= Quaternion.Euler(new Vector3(0.0f,0.0f,5.0f));
						puzzlePieceTransform = ObjectPuzzleManager.Instance.CurrentBatch.ClickedPiece.transform;
						puzzlePieceTransform.rotation		= Mathfx.Hermite(Quaternion.Euler(new Vector3(0.0f,0.0f,10.0f)),
						                                                Quaternion.Euler(new Vector3(0.0f,0.0f,-10.0f)),
						                                                _elpase);
						
					}
					
					else
					{
						puzzlePieceTransform.rotation		= Mathfx.Hermite(Quaternion.Euler(new Vector3(0.0f,0.0f,10.0f)),
						                                                Quaternion.Euler(new Vector3(0.0f,0.0f,-10.0f)),
						                                                _elpase);
					}
				}
				else
				{
					puzzlePieceTransform.rotation		= Mathfx.Hermite(Quaternion.Euler(new Vector3(0.0f,0.0f,10.0f  + rotationValue)),
					                                                Quaternion.Euler(new Vector3(0.0f,0.0f,-10.0f + rotationValue)),
					                                                _elpase);
				}
				break;
			case gameState.scenePuzzle:
				if(ScenePuzzleManager.Instance.CurrentBatch.ClickedPiece != null)
				{
					if(ScenePuzzleManager.Instance.CurrentBatch.ClickedPiece.transform != puzzlePieceTransform)
					{
						
						puzzlePieceTransform.rotation 		= Quaternion.Euler(new Vector3(0.0f,0.0f,5.0f));
						puzzlePieceTransform				= ScenePuzzleManager.Instance.CurrentBatch.ClickedPiece.transform;
						puzzlePieceTransform.rotation		= Mathfx.Hermite(Quaternion.Euler(new Vector3(0.0f,0.0f,10.0f)),
						                                                Quaternion.Euler(new Vector3(0.0f,0.0f,-10.0f)),
						                                                _elpase);
						
					}
					
					else
					{
						puzzlePieceTransform.rotation		= Mathfx.Hermite(Quaternion.Euler(new Vector3(0.0f,0.0f,10.0f)),
						                                                Quaternion.Euler(new Vector3(0.0f,0.0f,-10.0f)),
						                                                _elpase);
					}
				}
				else
				{
					puzzlePieceTransform.rotation		= Mathfx.Hermite(Quaternion.Euler(new Vector3(0.0f,0.0f,10.0f  + rotationValue)),
					                                                Quaternion.Euler(new Vector3(0.0f,0.0f,-10.0f + rotationValue)),
					                                                _elpase);
				}
				break;
		}
		
		
		
	}
	
	
}