﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Component for Hint Arrow
/// </summary>
public class HintArrow : HintBase 
{
	[SerializeField] 	private DottedLineRenderer	line;
	[SerializeField] 	private TranslateComponent	translateComponent;
	[SerializeField] 	private SpriteRenderer		arrowHead;

	[SerializeField] 	private float				objectPuzzleZdepth;
	[SerializeField] 	private float				scenePuzzleZdepth;

	private float depth;
	private ClickablePieceBase piece;

	private void Start()
	{
		translateComponent.TranslateEnd += HandleTranslateEnd;
		arrowHead.enabled = false;
		enabled = false;
	}

	void HandleTranslateEnd()
	{
		translateComponent.StartMoving();
	} 

	private void Update()
	{
		if(started)
		{
			switch(GameManager.Instance.GameState)
			{
				case gameState.objectPuzzle:
					if(ObjectPuzzleManager.Instance.CurrentBatch.ClickedPiece != piece)
					{
						piece = ObjectPuzzleManager.Instance.CurrentBatch.ClickedPiece;

						translateComponent.EndPos = piece.PlacementPiece.transform.position.ChangeZValue(depth-0.01f);
						translateComponent.StartMoving();

						line.Positions[0] = piece.PlacementPiece.transform.position.ChangeZValue(depth);
					}
					break;
				case gameState.scenePuzzle:
					if(ScenePuzzleManager.Instance.CurrentBatch.ClickedPiece != piece)
					{
						piece = ScenePuzzleManager.Instance.CurrentBatch.ClickedPiece;

						translateComponent.EndPos = piece.PlacementPiece.transform.position.ChangeZValue(depth-0.01f);
						translateComponent.StartMoving();

						line.Positions[0] = piece.PlacementPiece.transform.position.ChangeZValue(depth);
					}
					break;
			}
			
			translateComponent.StartPos = piece.transform.position.ChangeZValue(depth-0.01f);
			line.Positions[1] = piece.transform.position.ChangeZValue(depth);

		}
	}

	public override void ResumeHint()  
	{
		translateComponent.EndPos = piece.PlacementPiece.transform.position.ChangeZValue(objectPuzzleZdepth-0.01f);
		line.Positions[0] = piece.PlacementPiece.transform.position.ChangeZValue(objectPuzzleZdepth);
		line.Positions[1] = piece.transform.position.ChangeZValue(objectPuzzleZdepth);

		base.ResumeHint();
	}
	public override void PauseHint()
	{

	}
	public override void ClearHint()
	{
		base.ClearHint();
		translateComponent.enabled = false;
		enabled = false;
	}
	public override void StartHint(ClickablePieceBase _puzzlePiece)
	{
		base.StartHint(_puzzlePiece);
		
		piece = _puzzlePiece;

		switch(GameManager.Instance.GameState)
		{
			case gameState.objectPuzzle:	depth = objectPuzzleZdepth; break;
			case gameState.scenePuzzle:		depth = scenePuzzleZdepth; break;
		} 

		translateComponent.EndPos			= line.Positions[0] = piece.PlacementPiece.transform.position.ChangeZValue(depth);
		arrowHead.transform.localPosition = translateComponent.StartPos	= line.Positions[1] = piece.transform.position.ChangeZValue(depth);	

		translateComponent.StartMoving();

		line.enabled = true;
		translateComponent.enabled = true;
		arrowHead.enabled = true;

		enabled = true;
	}

	private void OnEnable()
	{
		line.enabled = true;
		translateComponent.enabled = true;
		arrowHead.enabled = true;
	}

	private void OnDisable()
	{
		line.enabled = false;
		translateComponent.enabled = false;
		arrowHead.enabled = false;
	}


}
