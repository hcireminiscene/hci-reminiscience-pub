﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Component for Hint 1 (glow)
/// </summary>
public class Hint1 : HintBase 
{
	[SerializeField] private Color objectPuzzleStartColor;
	[SerializeField] private Color objectPuzzleEndColor;

	[SerializeField] private Color objectPuzzleCompletionStartColor;
	[SerializeField] private Color objectPuzzleCompletionEndColor;

	[SerializeField] private Color scenePuzzleStartColor;
	[SerializeField] private Color scenePuzzleEndColor;


	private Color startCompletionColor;
	private Color endCompletionColor;
	private Color startColor;
	private Color endColor;

	private PingPongTimer timer;

	[SerializeField] private SpriteRenderer puzzlePieceRenderer;
	[SerializeField] private SpriteRenderer completionPieceRenderer;


	private void Start()
	{
		timer = GetComponent<PingPongTimer>();
		timer.TimerRunning += HandleTimerRunning;
		timer.enabled = false;
	}

	private void HandleTimerRunning(float _elapse)
	{
		if(!started) 
			return;
		switch(GameManager.Instance.GameState)
		{
		case gameState.objectPuzzle:
				if(ObjectPuzzleManager.Instance.CurrentBatch.ClickedPiece != null)
				{
					if(ObjectPuzzleManager.Instance.CurrentBatch.ClickedPiece.Image != puzzlePieceRenderer)
					{
//						Debug.Log("<color=red>Clicked on Another Piece</color>");
						puzzlePieceRenderer.material.SetColor("_OverlayColor", startColor);
						puzzlePieceRenderer			= ObjectPuzzleManager.Instance.CurrentBatch.ClickedPiece.Image;
						completionPieceRenderer.material.SetColor("_OverlayColor", startCompletionColor);
						completionPieceRenderer		= (ObjectPuzzleManager.Instance.CurrentBatch.ClickedPiece).PlacementPiece.GetComponent<SpriteRenderer>();
					}
				}

				puzzlePieceRenderer.material.SetColor("_OverlayColor", Color.Lerp(startColor,endColor,_elapse));
				completionPieceRenderer.material.SetColor("_OverlayColor", Color.Lerp(startCompletionColor,endCompletionColor,_elapse));
			break;
			case gameState.scenePuzzle:
				if(ScenePuzzleManager.Instance.CurrentBatch.ClickedPiece != null)
				{
					if(ScenePuzzleManager.Instance.CurrentBatch.ClickedPiece.Image != puzzlePieceRenderer)
					{
//						Debug.Log("<color=red>Clicked on Another Piece</color>");
						puzzlePieceRenderer.material.SetColor("_OverlayColor", startColor);
						puzzlePieceRenderer			= ScenePuzzleManager.Instance.CurrentBatch.ClickedPiece.Image;
						completionPieceRenderer.material.SetColor("_OverlayColor", startCompletionColor);
						completionPieceRenderer		= (ScenePuzzleManager.Instance.CurrentBatch.ClickedPiece).PlacementPiece.GetComponent<SpriteRenderer>();
					}
				}
				
				puzzlePieceRenderer.material.SetColor("_OverlayColor", Color.Lerp(startColor,endColor,_elapse));
				completionPieceRenderer.material.SetColor("_OverlayColor", Color.Lerp(startCompletionColor,endCompletionColor,_elapse));
				break;
		}

	}

	public override void PauseHint()	{	return;		}	/**< Don't pause **/
	public override void ResumeHint()	{	return;		}	/**< Don't pause **/

	public override void StartHint(ClickablePieceBase _puzzlePiece)
	{
		base.StartHint(_puzzlePiece);
		puzzlePieceRenderer		 = _puzzlePiece.Image;
		completionPieceRenderer	 = _puzzlePiece.PlacementPiece.GetComponent<SpriteRenderer>();

		if(GameManager.Instance.GameState == gameState.objectPuzzle)
		{
			startColor	= objectPuzzleStartColor;
			endColor 	= objectPuzzleEndColor;

			startCompletionColor = objectPuzzleCompletionStartColor;
			endCompletionColor   = objectPuzzleCompletionEndColor;
		}
		else if(GameManager.Instance.GameState == gameState.scenePuzzle)
		{
			startColor	= objectPuzzleStartColor;
			endColor 	= objectPuzzleEndColor;

			startCompletionColor = scenePuzzleStartColor;
			endCompletionColor	= scenePuzzleEndColor;
		}


		timer.enabled = true;
	}

	protected void OnDestory()
	{
		timer.TimerRunning -= HandleTimerRunning;
	}

	public override void ClearHint()
	{
		if(puzzlePieceRenderer != null)		puzzlePieceRenderer.material.SetColor("_OverlayColor",startColor);
		if(completionPieceRenderer != null) completionPieceRenderer.material.SetColor("_OverlayColor",startCompletionColor);

		timer.enabled = false;
		puzzlePieceRenderer		= null;
		completionPieceRenderer = null;
	}
}
