﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Component for Hint Bell Sound
/// </summary>
public class HintSound : HintBase
{
	private AudioSource audioSource;

	private void Start()
	{
		audioSource  = gameObject.Audio();

	}

	public override void ClearHint()
	{
		audioSource.Stop();
	}

	public void StartHint()
	{	
		if(!audioSource.isPlaying)
			audioSource.Play();

	}

	public override void StartHint(ClickablePieceBase _puzzlePiece)
	{
		StartHint();
	}
}
