﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : Singleton<SoundManager> 
{
	[SerializeField] private AudioClip opening;
	[SerializeField] private AudioClip bgm;
	[SerializeField,Range(0,1)] private float bgmVolume;

	[SerializeField] private AudioClip batchComplete;
	[SerializeField] private AudioClip objectPuzzleComplete;

	[SerializeField] private List<AudioSource> sourceList;

	private AudioClip changingClip;
	private float bgmPauseTime;
	private bool flag;
	private CountdownTimer timer;

	private void Start()
	{
		AudioSource bgmSource = gameObject.AddComponent<AudioSource>();
		timer = GetComponent<CountdownTimer>();

		bgmSource.clip = opening;
		bgmSource.loop = true;
		bgmSource.Play();

		bgmSource.volume =  bgmVolume;

		sourceList = new List<AudioSource>();
		sourceList.Add(bgmSource);

		timer.TimerRunning += (float _elapse) => 
		{
			if(!flag)		bgmSource.volume = (1-_elapse) * bgmVolume;
			else 			bgmSource.volume = _elapse * bgmVolume;
		};
		timer.TimerEnd += () => 
		{
			if(flag)
			{
				flag = false;
				bgmSource.volume = bgmVolume;

				timer.enabled = false;
				timer.ResetTimer();
			}
			else
			{
				float temp = bgmSource.time;
				bgmSource.clip = changingClip;
//				bgmSource.time = bgmPauseTime;
				bgmPauseTime = temp;
				bgmSource.Play();

				flag = true;
				timer.ResetTimer();
			}
		};

		timer.enabled = false;
	}

	public void PlayAndDestroy(AudioClip _clip)
	{
		AudioSource ao = gameObject.AddComponent<AudioSource>();
		ao.clip = _clip;
		ao.Play();
		sourceList.Add(ao);
		
		StartCoroutine("DestroyOnComplete",ao);
	}
	public void PlayBatchComplete()
	{
		PlayAndDestroy(batchComplete);
	}

	public void FadeBgm(gameState _state)
	{
		if(_state == gameState.objectPuzzle)
		{
			changingClip = bgm;
		}
		else if (_state == gameState.levelSelect)
		{
			changingClip = opening;
		}
		timer.enabled = true;
	}


	public void PlayObjectPuzzleComplete()
	{
		PlayAndDestroy(objectPuzzleComplete);
	}

	private IEnumerator DestroyOnComplete(AudioSource _source)
	{
		yield return new WaitForSeconds(_source.clip.length);
		sourceList.Remove(_source);
		Destroy(_source);
	}
}
