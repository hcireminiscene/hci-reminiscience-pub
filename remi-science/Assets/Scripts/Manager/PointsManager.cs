﻿using UnityEngine;
using System.Collections;

public class PointsManager : Singleton<PointsManager>
{
	[SerializeField] private uint pointsCollected;

	[SerializeField] private uint pointsPerTutorialBatch;
	[SerializeField] private uint pointsPer2PieceBatch;
	[SerializeField] private uint pointsPer3PieceBatch;
	[SerializeField] private float objectPuzzleModifer;
	[SerializeField] private uint pointsPerObjectPuzzleComplete;
	[SerializeField] private uint pointsPerSceneComplete;

	private uint currentPuzzleScore;

	public void BatchComplete(byte _numPerBatch,byte _totalPlayingPiece)
	{
		switch(_numPerBatch)
		{
			case 1:
				currentPuzzleScore += pointsPerTutorialBatch;
				break;
			case 2:
				if(_totalPlayingPiece == 3)		currentPuzzleScore += pointsPerTutorialBatch;
				else if(_totalPlayingPiece < 9)	currentPuzzleScore += pointsPer2PieceBatch;
				else							currentPuzzleScore += pointsPer3PieceBatch;
				break;
			case 3:
				currentPuzzleScore += pointsPer3PieceBatch;
				break;
		}
		pointsCollected += currentPuzzleScore;
		UIManager.Instance.UpdateMoneyCounter(pointsCollected,0);
	}

	public void ObjectPuzzleComplete()
	{
		
		pointsCollected +=  pointsPerObjectPuzzleComplete +	(uint)(currentPuzzleScore * objectPuzzleModifer);
		currentPuzzleScore = 0;
		UIManager.Instance.UpdateMoneyCounter(pointsCollected,0);
	}

	
	public void ScenePuzzleComplete()
	{
		pointsCollected += pointsPerSceneComplete;
		UIManager.Instance.UpdateMoneyCounter(pointsCollected,1);
	}

	public uint PointsCollected
	{
		get { return pointsCollected; }
	}

}
