﻿using UnityEngine;
using System.Collections;

public class EffectManager : Singleton<EffectManager>
{
	[SerializeField] private ParticleSystem	 coinBurstPref;

	[SerializeField] private ParticleSystem firework;
	[SerializeField] private ParticleSystem coinRaining;
	[SerializeField] private ParticleSystem[] coinBurstList;	

	private void Start()
	{
		coinBurstList = new ParticleSystem[3];
		for(int i=0;i<coinBurstList.Length;i++)
		{
			coinBurstList[i] = Instantiate(coinBurstPref);
			coinBurstList[i].name = "Coin Burst " + i;
			coinBurstList[i].transform.SetParent(this.transform);

		}
	}

	public ParticleSystem Firework
	{
		get { return firework; }
	}

	public void CoinBurst(System.Collections.Generic.List<ClickablePieceBase> _pieceList) 
	{
		for(int i=0;i<_pieceList.Count;i++)
		{
			if(!coinBurstList[i].isPlaying)
			{
				coinBurstList[i].transform.position = new Vector3(_pieceList[i].gameObject.transform.position.x,
				                                             	  _pieceList[i].gameObject.transform.position.y,
				                                                  coinBurstList[i].transform.position.z);
				coinBurstList[i].Play();
			}
		}
	}

	
	public void DropFullscreenCoins()
	{
		UIManager.Instance.SlideIn(1);
		PointsManager.Instance.ScenePuzzleComplete();

		coinRaining.Play();
		coinRaining.gameObject.Audio().Play();
		
		Invoke("CoinRainEnd",coinRaining.duration+coinRaining.startLifetime);
	}

	private void CoinRainEnd()
	{
		UIManager.Instance.SlideOut(1);

		ScenePuzzleManager.Instance.SlideOut();
		GameManager.Instance.WoodenFrame.SetActive(false);
		GameManager.Instance.GoToAlbum();
	}
}
