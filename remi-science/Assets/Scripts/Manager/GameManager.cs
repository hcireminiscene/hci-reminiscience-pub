using UnityEngine;
using System.Collections;


public class GameManager : Singleton<GameManager> 
{

	[System.Serializable] 
	private enum InputType	{	singleTouch,multiTouch	};

	[Header ("Game Options")]
	[SerializeField] private InputType		currentInput;
	[SerializeField] private GameMode 		gameMode;

	[Header ("Manager Variables")]
	[SerializeField] private gameState		state;
	[SerializeField] private bool 			tutorial;
	[SerializeField] private float 			resetDuration;
	[SerializeField] private float 			holdDuration;
	[SerializeField] private SceneSet[] 		sceneSetList;
	[SerializeField] private Rect 			playAreaLimit;

	[Header ("Other GameObjects Components")]
	[SerializeField] private Album			album;
	[SerializeField] private RectTransform 		woodenFrame;

	[SerializeField] private Vector2		objectPuzzleFrame;
	[SerializeField] private Vector2		scenePuzzleFrame;
	
	[Header ("Difficulties")]
	[SerializeField] private AdjustDifficultyChartData	adjustDifficultyChart;
	[SerializeField] private AdjustDifficultyChartData	tutorialDifficultyChart;
	[SerializeField] private AdjustDifficultyChartData	difficultyHintChart;

	[Header ("Exposed Variables(DEBUGGING)")]
	[SerializeField] private PlayerState 		currentPlayerState 	= PlayerState.idle;
	[SerializeField] private PlayerDifficult 	currentPlayerDifficulty	= PlayerDifficult.low;
	[SerializeField] private uint puzzleIndex;
	[SerializeField] private int  sceneIndex;
	[SerializeField] private uint currentNumOfPiece	= 3;

	
	[Header ("Manager Prefabs")]
	[SerializeField] private ObjectPuzzle	objectPuzzle;			// object puzzle pref
	[SerializeField] private ScenePuzzle	scenePuzzle;			// scene puzzle pref

	[Header ("Manager Components")]
	[SerializeField] private SlideTo raySlideTo;
	[SerializeField] private SlideTo cameraSlideTo;

	
	private uint previousHintRecieve;

	private void Start()
	{
		Application.targetFrameRate = 60;

		if(!Tutorial) {
			currentNumOfPiece = 4;
			ObjectPuzzleManager.Instance.UpdateInfo((byte)currentNumOfPiece); // for hardcoding if not tutorial
		}


		if(currentInput == InputType.singleTouch)
			Input.multiTouchEnabled = false;		// disable multi touch
		puzzleIndex = 0;
		woodenFrame.gameObject.SetActive(false);
		Screen.fullScreen = true;
		album.IsClickable = true;
	
		holdDuration = resetDuration;

		cameraSlideTo.ReachPosition += () =>
		{
			if(state == gameState.objectPuzzle)
			{
				ObjectPuzzleManager.Instance.Resume();
			}
			if(state == gameState.levelSelect)
			{
				ScenePuzzleManager.Instance.SlideOut();
				album.StartAlbumSequence();

				foreach(ParticleSystem ps in raySlideTo.GetComponentsInChildren<ParticleSystem>())
				{
					ps.Play();
				}
				raySlideTo.IsInverse = true;
				raySlideTo.Slide();
			}
		};

	}

	/// <summary>
	/// Update Method - For this application we only look for the back button (mapped to "Escape Key") to restart the game
	/// </summary>
	private void Update()
	{
		if(Input.GetKey(KeyCode.Escape))
		{
			if(holdDuration < 0.0f)
			{
				ResetSceneSet();
				Application.LoadLevel(Application.loadedLevel);
			}
			else
				holdDuration -= Time.deltaTime;
		}
		if(Input.GetKeyUp(KeyCode.Escape))
		{
			holdDuration =	resetDuration;
		}
	}

	private void CreateScenePuzzle()
	{
		GameData.Instance.ResetData();

		scenePuzzle.ScenePuzzleSprite = sceneSetList[sceneIndex].ScenePuzzleImage;

		for(int i=0;i<3;i++)
		{
			scenePuzzle.ScenePuzzlePieceSpriteList[i] 	= sceneSetList[sceneIndex].PuzzleAnimation[sceneSetList[sceneIndex].ScenePuzzleAnimationOrder[i]].spriteSheet[0];
			scenePuzzle.ScenePuzzleAnimation[i].SpriteSheet	= sceneSetList[sceneIndex].PuzzleAnimation[sceneSetList[sceneIndex].ScenePuzzleAnimationOrder[i]].spriteSheet;
			scenePuzzle.ScenePuzzleTransformAnimation[i].TransformData = sceneSetList[sceneIndex].TransformData[sceneSetList[sceneIndex].ScenePuzzleAnimationOrder[i]];


			scenePuzzle.ScenePuzzleAnimation[i].SetAudio(sceneSetList[sceneIndex].PuzzleSounds[sceneSetList[sceneIndex].ScenePuzzleAnimationOrder[i]],
			                                             sceneSetList[sceneIndex].PuzzleAudioCue[sceneSetList[sceneIndex].ScenePuzzleAnimationOrder[i]]);
		}
		ScenePuzzleManager.Instance.CreatePuzzle(scenePuzzle);
	}

	public void CreatePuzzle()
	{
		GameData.Instance.ResetData();
		UIManager.Instance.HideButtons();

		//TODO: Create PlayerPref for Puzzle
		PlayerPrefs.SetInt("Set_"+SceneSet.name+"_ob"+puzzleIndex+"_numPuzzle",(int)ObjectPuzzleManager.Instance.NumOfPiece);

		foreach(ParticleSystem ps in raySlideTo.GetComponentsInChildren<ParticleSystem>())
		{
			ps.Stop();
		}
		raySlideTo.IsInverse = false;
		raySlideTo.Slide();

		woodenFrame.sizeDelta =  objectPuzzleFrame;
		woodenFrame.gameObject.SetActive(true);

		if(ObjectPuzzleManager.Instance.PlayingPuzzle == null)
		{
			//Debug.Log("Creating Puzzle: " + puzzleIndex);


			objectPuzzle.PuzzleImage		= sceneSetList[sceneIndex].ObjectPuzzleImage[puzzleIndex];
			objectPuzzle.CompletionSound		= sceneSetList[sceneIndex].PuzzleSounds[puzzleIndex];
			objectPuzzle.AnimationAudioCueIndex	= sceneSetList[sceneIndex].PuzzleAudioCue[puzzleIndex];
			objectPuzzle.TransformData		= sceneSetList[sceneIndex].TransformData[puzzleIndex];
			objectPuzzle.AlbumImage			=  Sprite.Create( sceneSetList[sceneIndex].ObjectPuzzleImage[puzzleIndex],new Rect(0,0,1024,1024),new Vector2(0.5f,0.5f) );

			if(sceneSetList[sceneIndex].PuzzleAnimation.Length > puzzleIndex)
			{
				if(sceneSetList[sceneIndex].PuzzleAnimation[puzzleIndex].spriteSheet != null)
				{
					objectPuzzle.AnimationSpriteSheet = sceneSetList[sceneIndex].PuzzleAnimation[puzzleIndex].spriteSheet;
					objectPuzzle.AnimationBackground  = sceneSetList[sceneIndex].SpriteAnimationBackground[puzzleIndex];
				}
			}
			else
			{
				objectPuzzle.AnimationSpriteSheet = null;
				objectPuzzle.AnimationBackground  = null;
			}


			cameraSlideTo.IsInverse = true;
			cameraSlideTo.Slide();

			ObjectPuzzleManager.Instance.CreatePuzzle(objectPuzzle);
		}
	}

	public void NextPuzzle()
	{
		puzzleIndex++;

		Destroy(ObjectPuzzleManager.Instance.PlayingPuzzle.gameObject);
		ObjectPuzzleManager.Instance.PlayingPuzzle = null;

		if(sceneSetList[sceneIndex].ObjectPuzzleImage.Length <= puzzleIndex)
		{
			puzzleIndex = 0;
			state = gameState.scenePuzzle;

			woodenFrame.sizeDelta =  scenePuzzleFrame;
			woodenFrame.gameObject.SetActive(true);
			CreateScenePuzzle();
		}
		else
		{
			if(puzzleIndex == 2)
				tutorial = false;
			
			CreatePuzzle();
		}

	}


	public void UpdateScore(uint _previousHint,uint _currentNumOfPiece)
	{
		previousHintRecieve	 = _previousHint;
		currentNumOfPiece	 = _currentNumOfPiece;

		for(int i=0;i<difficultyHintChart.generatedChart.Length;i++)
		{
			if(difficultyHintChart.generatedChart[i].numOfPiece == currentNumOfPiece)
			{
				if(previousHintRecieve < difficultyHintChart.generatedChart[i].chart.increaseChart)		currentPlayerDifficulty = PlayerDifficult.high;
				else if(previousHintRecieve < difficultyHintChart.generatedChart[i].chart.remainChart)	currentPlayerDifficulty = PlayerDifficult.mid;
				else																					currentPlayerDifficulty = PlayerDifficult.low;
			}
		}
		sceneSetList[sceneIndex].StarAward[puzzleIndex] = (byte)(currentPlayerDifficulty+1);
		AdjustDifficulty();
	}


	private void AdjustDifficulty()
	{
		if(tutorial)
		{
			foreach(AdjustDifficultyChart chart in tutorialDifficultyChart.generatedChart)
			{
				if(chart.numOfPiece == currentNumOfPiece)
				{
					switch(currentPlayerDifficulty)
					{
					case PlayerDifficult.low:	ObjectPuzzleManager.Instance.UpdateInfo(chart.chart.decreaseChart); break;
					case PlayerDifficult.mid:	ObjectPuzzleManager.Instance.UpdateInfo(chart.chart.remainChart); break;
					case PlayerDifficult.high:	ObjectPuzzleManager.Instance.UpdateInfo(chart.chart.increaseChart); break;
					}
					
					return;
				}
			}
		}
		else
		{
			foreach(AdjustDifficultyChart chart in adjustDifficultyChart.generatedChart)
			{
				if(chart.numOfPiece == currentNumOfPiece)
				{
					switch(currentPlayerDifficulty)
					{
					case PlayerDifficult.low:	ObjectPuzzleManager.Instance.UpdateInfo(chart.chart.decreaseChart); break;
					case PlayerDifficult.mid:	ObjectPuzzleManager.Instance.UpdateInfo(chart.chart.remainChart); break;
					case PlayerDifficult.high:	ObjectPuzzleManager.Instance.UpdateInfo(chart.chart.increaseChart); break;
						
					}
					return;
				}
				
			}
		}
	
	}
	
	public void GoToAlbum()
	{
		album.gameObject.SetActive(true);
		album.IsClickable = false;
		album.Completed();
		sceneSetList[sceneIndex].Completed = true;
		
		state = gameState.levelSelect;
		woodenFrame.gameObject.SetActive(false);


		cameraSlideTo.IsInverse = false;
		cameraSlideTo.Slide();

		
		SoundManager.Instance.FadeBgm(gameState.levelSelect);
	}


	public PlayerState CurrentPlayerState
	{
		set {	currentPlayerState = value;	}
		get {	return currentPlayerState;	}
	}

	public bool Tutorial
	{
		get { return tutorial; }
	}

	public SceneSet SceneSet
	{
		get { return sceneSetList[sceneIndex]; }
	}

	public void NextSet()
	{
		sceneIndex++;
		if(sceneIndex > sceneSetList.Length-1)
		{
			sceneIndex = 0;
		}
	}

	public void PreviousSet()
	{
		sceneIndex--;
		if(sceneIndex < 0)
		{
			sceneIndex = sceneSetList.Length -1;
		}
	}

	public PlayerDifficult CurrentPlayerDifficulty
	{
		get { return currentPlayerDifficulty; }
	}
	public gameState GameState
	{
		get { return state;	}
		set { state = value; }
	}

	public GameMode GameMode
	{
		get {	return gameMode;	}	
	}
	public int PuzzleIndex
	{
		get { return (int)puzzleIndex; }
	}

	public void ChangeMode()
	{
		gameMode++;
		if(gameMode > GameMode.autoSnap)
			gameMode = 0;
	}

	public GameObject WoodenFrame 	{	get { return woodenFrame.gameObject;	} }
	public Rect PlayAreaLimit	{	get { return playAreaLimit; 		} }

	public void ResetSceneSet()
	{
		foreach(SceneSet ss in sceneSetList)
		{
			ss.Reset();
#if UNITY_EDITOR || UNITY_EDITOR_OSX
			UnityEditor.EditorUtility.SetDirty(ss);
#endif
		}
	}

	/// <summary>
	/// Drawing Playable Area in Unity Scene
	/// </summary>
	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.green;
		Gizmos.DrawLine( new Vector2(playAreaLimit.xMin,playAreaLimit.yMin),
		                new Vector2(playAreaLimit.xMax,playAreaLimit.yMin)
		                );
		Gizmos.DrawLine( new Vector2(playAreaLimit.xMin,playAreaLimit.yMax),
		                new Vector2(playAreaLimit.xMax,playAreaLimit.yMax)
		                );
		Gizmos.DrawLine( new Vector2(playAreaLimit.xMin,playAreaLimit.yMin),
		                new Vector2(playAreaLimit.xMin,playAreaLimit.yMax)
		                );
		Gizmos.DrawLine( new Vector2(playAreaLimit.xMax,playAreaLimit.yMin),
		                new Vector2(playAreaLimit.xMax,playAreaLimit.yMax)
		                );
		
	}
}