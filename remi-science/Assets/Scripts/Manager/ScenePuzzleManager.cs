﻿using UnityEngine;
using System.Collections;

public class ScenePuzzleManager : Singleton<ScenePuzzleManager>
{
	private ScenePuzzle			scenePuzzle;

	[SerializeField] private Batch					batchPref;
	[SerializeField] private ScenePiece				scenePiecePref;
	[SerializeField] private SceneCompletionPiece	sceneCompletionPiecePref;
	[SerializeField] private ScenePlayingPiece		scenePlayingPiecePref;
	[SerializeField] private PuzzleSetCoord			coordinate;
	[SerializeField] private Vector3				sceneCompletionPosition;

	[SerializeField] private HintTiming[] hintTimingList;
	[SerializeField] private int hintsRecieved;
	[SerializeField] private HintBase[] hintList;
	
	private CountupTimer hintIdleNotPlaceTiming;
	private CountupTimer hintTouchNotPlaceTiming;


	[SerializeField] private AudioClip[]			clips;

	private ScenePiece[]			scenePieceList;
	private SceneCompletionPiece[] 	sceneCompletionPieceList;
	private Batch					currentBatch;

	private void Start()
	{
		scenePieceList = new ScenePiece[coordinate.numOfPiece];

		CountupTimer[] timers = GetComponents<CountupTimer>();
		
		hintIdleNotPlaceTiming = timers[0];
		hintTouchNotPlaceTiming = timers[1];
		
		hintIdleNotPlaceTiming.TimerRunning 		+= HandleIdlingTimerRunning;
		hintTouchNotPlaceTiming.TimerRunning		+= HandleTouchTimerRunning;
		
		hintIdleNotPlaceTiming.enabled		= false;
		hintTouchNotPlaceTiming.enabled		= false;

	}
	

	public void CompletePiece(ScenePlayingPiece _piece)
	{
		currentBatch.PieceSolved((ClickablePieceBase)_piece);


		// deactive both playing piece and placement piece game objects
		_piece.gameObject.SetActive(false);
		_piece.PlacementPiece.gameObject.SetActive(false);

		for(int i=0;i<scenePuzzle.ScenePuzzleAnimation.Length;i++)
		{
			if(scenePuzzle.ScenePuzzleAnimation[i].SpriteSheet[0] == _piece.Image.sprite)
			{
				scenePuzzle.ScenePuzzleAnimation[i].gameObject.SetActive(true);
			}
		}
		//scenePiece.CompletePiece(1-(float)(sceneCompletionPieceList.Length - playingBatch.PuzzleList.Count)/sceneCompletionPieceList.Length);

		for(int i=0;i<scenePieceList.Length;i++)
		{
			if(scenePieceList[i].PlayPiece == _piece)
			{
				scenePieceList[i].CompletePiece(0.0f);
				return;
			}
		}


	}

	public void CreatePuzzle(ScenePuzzle _puzzle)
	{
		scenePuzzle = Instantiate(_puzzle) as ScenePuzzle;
		scenePuzzle.transform.SetParent(this.transform);
		scenePuzzle.name = "Scene Puzzle";


		GameObject sceneCompletionHolder = new GameObject();
		sceneCompletionHolder.transform.SetParent(scenePuzzle.transform);
		sceneCompletionHolder.name = "Scene Completion Pieces";
		sceneCompletionHolder.transform.localScale    = new Vector3(0.99f,0.99f,1.0f);
		sceneCompletionHolder.transform.localPosition = sceneCompletionPosition;

		FileManager.Instance.SaveText("Scene Puzzle");
		FileManager.Instance.SaveText("Index",
		                              "Idle Timing",
		                              "Piece Timing",
		                              "Hints Recieved");

		for(int i=0;i<coordinate.numOfPiece;i++)
		{
			scenePieceList[i] = Instantiate(scenePiecePref) as ScenePiece;
			scenePieceList[i].Image.sprite = Sprite.Create(_puzzle.ScenePuzzleSprite.texture,coordinate.dimension,Vector2.one/2,140.0f);
			scenePieceList[i].Image.sprite.OverrideGeometry(coordinate.piece[i].vertCoord,coordinate.piece[i].vertTriangles);

			scenePieceList[i].transform.SetParent(sceneCompletionHolder.transform);
			scenePieceList[i].transform.localScale = Vector3.one;
			scenePieceList[i].transform.position = Vector3.zero;
			scenePieceList[i].transform.localPosition = new Vector3(0.0f,0.5f,0.5f);
		}

		currentBatch = Instantiate(batchPref);
		currentBatch.name 			= "batch";
		currentBatch.transform.SetParent(scenePuzzle.transform);
		currentBatch.transform.localPosition = new Vector3(16.0f,0.0f,0.0f);


		sceneCompletionPieceList	= new SceneCompletionPiece[scenePuzzle.ScenePuzzlePieceSpriteList.Length];

	
		// create  pieces
		for(int i=0;i<scenePuzzle.ScenePuzzlePieceSpriteList.Length;i++)
		{
			sceneCompletionPieceList[i] = Instantiate(sceneCompletionPiecePref) as SceneCompletionPiece;
			sceneCompletionPieceList[i].Image.sprite = scenePuzzle.ScenePuzzlePieceSpriteList[i];
			sceneCompletionPieceList[i].name = "Scene Completion Piece " + i;
			sceneCompletionPieceList[i].transform.SetParent(scenePuzzle.transform);

			sceneCompletionPieceList[i].SetRotation(GameManager.Instance.SceneSet.RotationList[i]);
			sceneCompletionPieceList[i].SetScale(GameManager.Instance.SceneSet.ScaleList[i]);
			sceneCompletionPieceList[i].transform.localPosition = GameManager.Instance.SceneSet.PositionList[i];

			// Playing Piece
			ScenePlayingPiece temp = Instantiate(scenePlayingPiecePref) as ScenePlayingPiece;
			scenePieceList[i].PlayPiece = temp;
			temp.Image.sprite 	= scenePuzzle.ScenePuzzlePieceSpriteList[i];
			temp.name 			= "Scene Playing Piece " + i;
			temp.transform.SetParent(currentBatch.transform);
			temp.transform.position = Vector3.zero;
			temp.PlacementPiece 	= sceneCompletionPieceList[i];

		
			temp.SetEndRotation(GameManager.Instance.SceneSet.RotationList[i]);
			temp.SetEndScale(GameManager.Instance.SceneSet.ScaleList[i]);

			switch(i)
			{
			case 0:		temp.transform.localPosition = new Vector3(0.0f,3.5f,0.0f);		break;
			case 1:		temp.transform.localPosition = new Vector3(0.0f,0.0f,0.0f);		break;
			case 2:		temp.transform.localPosition = new Vector3(0.0f,-3.5f,0.0f);	break;
			}

			currentBatch.PuzzleList.Add(temp); 
		}

		for(int i=0;i<scenePuzzle.ScenePuzzleAnimation.Length;i++)
		{
			scenePuzzle.ScenePuzzleAnimationOffset[GameManager.Instance.SceneSet.ScenePuzzleAnimationOrder[i] ].transform.localPosition	= GameManager.Instance.SceneSet.PositionList[ GameManager.Instance.SceneSet.ScenePuzzleAnimationOrder[i] ];
			scenePuzzle.ScenePuzzleAnimationOffset[GameManager.Instance.SceneSet.ScenePuzzleAnimationOrder[i] ].transform.localScale		= GameManager.Instance.SceneSet.ScaleList[ GameManager.Instance.SceneSet.ScenePuzzleAnimationOrder[i] ];
			scenePuzzle.ScenePuzzleAnimationOffset[GameManager.Instance.SceneSet.ScenePuzzleAnimationOrder[i] ].transform.rotation		= GameManager.Instance.SceneSet.RotationList[ GameManager.Instance.SceneSet.ScenePuzzleAnimationOrder[i] ];
		}

		currentBatch.SetEndPos(new Vector3(7.0f,0.0f,-4.0f));

		scenePuzzle.SlideTo.IsInverse 		= false;
		scenePuzzle.SlidePuzzle();

		scenePuzzle.PlayAudio();
	}

	public void BatchSolved()
	{
		if(currentBatch.Completed)
		{
			//EffectManager.instance.DropFullscreenCoins();
			// scale up
			scenePuzzle.Complete();

			ResetHint();
			hintIdleNotPlaceTiming.enabled	= false;
			hintTouchNotPlaceTiming.enabled 		= false;
		}
	}

	public void SlideOut()
	{
		if(scenePuzzle != null)
		{
			scenePuzzle.SlideTo.IsInverse 		= true;
			scenePuzzle.SlidePuzzle();
		}
	}

	public Batch CurrentBatch
	{
		get { return currentBatch; }
	}

	public int HintsRecieved
	{
		get { return hintsRecieved; }
	}

	
	public void StartHintTimer()
	{
		hintIdleNotPlaceTiming.enabled = true;
		hintIdleNotPlaceTiming.ResetTimer();
		
		hintTouchNotPlaceTiming.enabled = true;
		hintTouchNotPlaceTiming.ResetTimer();
	}
	
	public void ResetIdleHintTimer()
	{
		hintIdleNotPlaceTiming.ResetTimer();
	}

	public void PauseHints()
	{
		for(int i=0;i<hintsRecieved;i++)
		{
			hintList[i].PauseHint();
		}
		
	}
	
	
	public void ResumeHints()
	{
		for(int i=0;i<hintsRecieved;i++)
		{
			hintList[i].ResumeHint();
		}
	}
	
	public void ResetHint(int _index)
	{
		hintList[_index].ClearHint();
	}
	
	public void ResetHint()
	{
		hintIdleNotPlaceTiming.ResetTimer();
		hintTouchNotPlaceTiming.ResetTimer();
		for(int i=0;i<hintList.Length;i++)
		{
			hintList[i].ClearHint();
		}
		hintsRecieved = 0;
	}


	private void HandleIdlingTimerRunning(float _elapse)
	{
		if(hintsRecieved >= hintTimingList.Length) return;
		if(_elapse > hintTimingList[hintsRecieved].idleNotPlaceTiming)
		{
			hintList[hintsRecieved].StartHint((ClickablePieceBase)currentBatch.RandomClickablePiece());
			hintsRecieved++;
			
			hintIdleNotPlaceTiming.ResetTimer();
			hintTouchNotPlaceTiming.ResetTimer();
		}
		
		
	}
	
	private void HandleTouchTimerRunning (float _elapse)
	{
		if(hintsRecieved >= hintTimingList.Length) return;
		if(_elapse > hintTimingList[hintsRecieved].touchNotPlaceTiming && hintTimingList[hintsRecieved].touchNotPlaceTiming != 0 && GameManager.Instance.CurrentPlayerState == PlayerState.touchNotMove)
		{
			hintList[hintsRecieved].StartHint((ClickablePieceBase)currentBatch.RandomClickablePiece());
			hintsRecieved++;
			
			hintIdleNotPlaceTiming.ResetTimer();
			hintTouchNotPlaceTiming.ResetTimer();
		}
	}
}
