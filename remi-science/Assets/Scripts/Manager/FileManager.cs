using UnityEngine;
using System.Collections;

public class FileManager : Singleton<FileManager> 
{

	public static int playerIndex	= 0;
	public static int fileIndex 	= 0;

	private		CountupTimer sessionTimer;
	private 	CountupTimer interactiveTimer;
	private  	System.Text.StringBuilder dataGrid = new System.Text.StringBuilder();
	private 	System.Text.StringBuilder prevDataGrid = new System.Text.StringBuilder();

	private float prevTotalTime;
	private float totalTime;


	private void Start()
	{
		sessionTimer		= GetComponents<CountupTimer>()[0];
		interactiveTimer	= GetComponents<CountupTimer>()[1];


		if(PlayerPrefs.GetInt("playerIndex") != 0)
		{
			playerIndex = PlayerPrefs.GetInt("playerIndex");
		}
		
		CreateFolder();
		PlayerPrefs.SetInt("playerIndex",playerIndex);
		fileIndex = 0;

		interactiveTimer.enabled	= false;
		sessionTimer.enabled		= false;
	}

	/// <summary>
	/// Create Player Folder
	/// </summary>
	private void CreateFolder()
	{
		System.IO.Directory.CreateDirectory(Application.persistentDataPath + "/Player " + playerIndex.ToString());
		Debug.Log(Application.persistentDataPath + "/Player " + playerIndex.ToString());

		playerIndex++;
	}

	/// <summary>
	/// Create Files per sets
	/// </summary>
	public void CreateFile()
	{
		dataGrid.Clear();
		if(!System.IO.File.Exists(Application.persistentDataPath + "/Player " + (playerIndex-1).ToString() + "/Set_" + GameManager.Instance.SceneSet.name + ".csv")) 
		   System.IO.File.Create(Application.persistentDataPath + "/Player " + (playerIndex-1).ToString() + "/Set_" + GameManager.Instance.SceneSet.name + ".csv" );

		//Debug.Log(Application.persistentDataPath + "/Player " + (playerIndex-1).ToString() + "/Set " + fileIndex + ".csv");
		fileIndex++;
	}

	public void NewLineInStringBuilder()
	{
		dataGrid.AppendLine(",");
	}

	public void StartInteractiveTimer()
	{
		interactiveTimer.enabled = true;
	}

	public void StartSessionTimer()
	{
		sessionTimer.enabled = true;
	}

	public void EndInteractiveTimer()
	{
		SaveText(
				 "Interactive Time",
				 System.TimeSpan.FromSeconds(interactiveTimer.CurrentTime).Minutes.ToString() + "m:" +
				 System.TimeSpan.FromSeconds(interactiveTimer.CurrentTime).Seconds.ToString() + "s:" +
				 System.TimeSpan.FromSeconds(interactiveTimer.CurrentTime).Milliseconds.ToString() + "ms"
				 );
		NewLineInStringBuilder();

		interactiveTimer.ResetTimer();
		interactiveTimer.enabled = false;

		WriteToFile();
	}

	public void EndSessionTimer()
	{
		SaveText(
					"Session Time",
					System.TimeSpan.FromSeconds(sessionTimer.CurrentTime).Minutes.ToString() + "m:" +
					System.TimeSpan.FromSeconds(sessionTimer.CurrentTime).Seconds.ToString() + "s:" +
					System.TimeSpan.FromSeconds(sessionTimer.CurrentTime).Milliseconds.ToString() + "ms"
		         );

		totalTime += sessionTimer.CurrentTime;

		sessionTimer.ResetTimer();
		sessionTimer.enabled = false;

		WriteToFile();
	}

	public void SaveText(params object[] _object)
	{
		for(int i=0;i<_object.Length;i++)
		{
			dataGrid.Append(_object[i].ToString());
			dataGrid.Append(",");
		}
		dataGrid.AppendLine();
		Debug.Log(dataGrid.ToString());
	}

	private void OnDestroy()
	{
		SaveText("Application Resetting");
		ForceClose();
	}

	private void OnApplicationPause(bool _pauseStatus)
	{
		if(_pauseStatus)
		{
			prevDataGrid = new System.Text.StringBuilder( dataGrid.ToString() );
			prevTotalTime = totalTime;
			SaveText("Application Quit");
			
			ForceClose();
		}
		else
		{
			totalTime = prevTotalTime;
			dataGrid = new System.Text.StringBuilder( prevDataGrid.ToString() );
		}
	}

	public void ForceClose()
	{
		if(fileIndex == 0) 
		{
			return;
		}

		if(sessionTimer.enabled)
		{
			Debug.Log("Force Reset");
			NewLineInStringBuilder();
			SaveText(
				"Session Time",
				System.TimeSpan.FromSeconds(sessionTimer.CurrentTime).Minutes.ToString() + "m : " +
				System.TimeSpan.FromSeconds(sessionTimer.CurrentTime).Seconds.ToString() + "s : " +
				System.TimeSpan.FromSeconds(sessionTimer.CurrentTime).Milliseconds.ToString() + "ms"
				);
			NewLineInStringBuilder();
			totalTime += sessionTimer.CurrentTime;
			SaveText("Total Session Time",
			         System.TimeSpan.FromSeconds(totalTime).Minutes.ToString() + "m : " +
			         System.TimeSpan.FromSeconds(totalTime).Seconds.ToString() + "s : " +
			         System.TimeSpan.FromSeconds(totalTime).Milliseconds.ToString() + "ms"
			         );
			
			System.IO.File.WriteAllText(Application.persistentDataPath + "/Player " + (playerIndex-1).ToString() + "/Set_" + GameManager.Instance.SceneSet.name + ".csv",dataGrid.ToString());
		}
	}

	public void WriteToFile()
	{
		System.IO.File.WriteAllText(Application.persistentDataPath + "/Player " + (playerIndex-1).ToString() + "/Set_" + GameManager.Instance.SceneSet.name + ".csv",dataGrid.ToString());
	}
}
