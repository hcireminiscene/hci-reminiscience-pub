﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;
using System.Collections.Generic;


public class ObjectPuzzleManager : Singleton<ObjectPuzzleManager>
{
	[SerializeField,Range(3, 16)] private byte numOfPieces;				// reduce cost
	[SerializeField,Range(1, 3)] private byte piecesPerBatch;			// number of pieces per batch

	[SerializeField] private ObjectPuzzlePiece 	puzzlePiecePref;		// pref for puzzle piece
	[SerializeField] private CompletionPiece	completionPiecePref;		// pref for completion piece
	[SerializeField] protected Batch		batchPref;			// pref for batch
	
	[SerializeField] private PuzzleSetCoordList	puzzleSetCoordList;		// puzzle set list
	[SerializeField] private CompletionPiece[] 	completionSlices;		// complete piece List

	[SerializeField] private Vector3 		completionPieceHolderPosition;

	[SerializeField] private HintTiming[] hintTimingList;
	[SerializeField] private int hintsRecieved;
	[SerializeField] private HintBase[] hintList;
	
	private CountupTimer hintIdleNotPlaceTimer;
	private CountupTimer hintTouchNotPlaceTimer;

	private uint	totalHintsRecieved;

	private StarRank 	 starRank;							// Star Ranking System
	private ObjectPuzzle objectPuzzle;							// playing puzzle
	[SerializeField] private Batch  		currentBatch;				// current Batch
	private Queue<Batch> batchList	= new Queue<Batch>();					// batch list

	[SerializeField] private Rotation[] 		rotationDifficulty;
	[SerializeField] private DifficultyChart	rotationChart;
	[SerializeField] private RotationType		currentRotationType;
	[SerializeField] private AudioClip[]		clips;

	private Vector3 currentRotation;
	[SerializeField, Range(0,1)] private float	playingPieceDistanceChecker;

	private void Start()
	{
		CountupTimer[] timers = GetComponents<CountupTimer>();

		hintIdleNotPlaceTimer	= timers[0];
		hintTouchNotPlaceTimer	= timers[1];

		hintIdleNotPlaceTimer.TimerRunning 		+= HandleIdleHintRunning;
		hintTouchNotPlaceTimer.TimerRunning		+= HandleTouchHintRunning;

		hintIdleNotPlaceTimer.enabled		= false;
		hintTouchNotPlaceTimer.enabled		= false;

		starRank = GetComponentInChildren<StarRank>();

	}

	/// <summary>
	/// Used by Game Manager to Update object puzzle information based on some Difficulty specified in Difficulty Chart / Tutorial Chart (if it is the first puzzle played)
	/// </summary>
	/// <param name="_numOfPiece">Number of piece for next puzzle.</param>
	public void UpdateInfo(byte _numOfPiece)
	{
		numOfPieces = _numOfPiece;
		if(numOfPieces <= rotationChart.decreaseChart)
		{
			currentRotationType = RotationType.low;
			if((numOfPieces == 4 || numOfPieces == 6) && GameManager.Instance.CurrentPlayerDifficulty == PlayerDifficult.low )
			{
				if(numOfPieces == 6 && PiecePerBatch == 1)
				{
					numOfPieces = 4;
					piecesPerBatch = 2;
					return;
				}
				piecesPerBatch = 1;
			}
			else
				piecesPerBatch = 2;
		}
		else if (numOfPieces <= rotationChart.remainChart)
		{
			currentRotationType = RotationType.mid;
			piecesPerBatch = 2;
		}
		else
		{
			currentRotationType = RotationType.high;
			piecesPerBatch = 2;
		}
	}


	/// <summary>
	/// Creates Object Puzzle - Create all Completion ObjectPuzzleCompletionPiece and Batches of ObjectPuzzlePiece for this Object Puzzle.
	/// </summary>
	/// <param name="_puzzle">_puzzle.</param>
	public void CreatePuzzle(ObjectPuzzle _puzzle)
	{	

		switch(currentRotationType)
		{
		case RotationType.low:	currentRotation = Utility.RandomRange(rotationDifficulty[0].lowRange,rotationDifficulty[0].highRange); break;
		case RotationType.mid:	currentRotation = Utility.RandomRange(rotationDifficulty[1].lowRange,rotationDifficulty[1].highRange); break;
		case RotationType.high:	currentRotation = Utility.RandomRange(rotationDifficulty[2].lowRange,rotationDifficulty[2].highRange); break;
		}
		//Debug.Log(currentRotation);

		FileManager.Instance.SaveText("Number of Piece",numOfPieces);
		FileManager.Instance.SaveText("Index",
		                              "Idle Timing",
		                              "Piece Timing",
		                              "Hints Received");

		totalHintsRecieved = 0;
		// Create Puzzle Sprites
		objectPuzzle = Instantiate(_puzzle) as ObjectPuzzle;
		objectPuzzle.name = "Puzzle";
		objectPuzzle.transform.parent = this.transform;

		Debug.Log(numOfPieces);

		// create object puzzle based on number of puzzle
		objectPuzzle.CreateSliceSprite(puzzleSetCoordList.RandomCoordinate(numOfPieces));


		// Create the pieces based on Sprites
		completionSlices = new CompletionPiece[numOfPieces];

		int batchIndex = 0;
		Batch batchTemp = null;
		GameObject completionPieceHolder = new GameObject();
		completionPieceHolder.name = "Completion Pieces";
		completionPieceHolder.transform.parent = objectPuzzle.transform;
		completionPieceHolder.transform.localPosition = completionPieceHolderPosition;

		if(numOfPieces != 3)
		{
			for (int i=0;i<numOfPieces;i++)
			{
				completionSlices[i] = Instantiate(completionPiecePref) as CompletionPiece;
				completionSlices[i].name = "Completion Field " + i;
				completionSlices[i].transform.parent = completionPieceHolder.transform;
				completionSlices[i].transform.localPosition = Vector3.zero;
				completionSlices[i].CreatePiece(objectPuzzle.SpriteList[i]);
				
				if(batchIndex == 0)
				{
					batchTemp = Instantiate(batchPref) as Batch;
					batchTemp.name = "Batch " + i/piecesPerBatch;
					batchTemp.transform.parent = objectPuzzle.transform;
					batchTemp.transform.localPosition =  new Vector3(20.0f,0.0f,0.0f);
					batchList.Enqueue(batchTemp);
				}
				
				ObjectPuzzlePiece playingSlice = null;
				
				playingSlice		= Instantiate(puzzlePiecePref) as ObjectPuzzlePiece;
				playingSlice.name	= "Playing Piece " + i;
				playingSlice.CreatePiece(objectPuzzle.SpriteList[i]);
				playingSlice.PlacementPiece = completionSlices[i];
				playingSlice.transform.parent = batchTemp.transform;
				playingSlice.transform.localPosition = Vector3.zero;
				playingSlice.transform.rotation = Quaternion.Euler( currentRotation );
				playingSlice.IsClickable = false;

				switch(i%piecesPerBatch)
				{
				case 0:		playingSlice.transform.position += (Vector3.forward/10)*(i%piecesPerBatch) + new Vector3(-2,3,0);		break;
				case 1:		playingSlice.transform.position += (Vector3.forward/10)*(i%piecesPerBatch) + new Vector3(2,0,0);		break;
				case 2:		playingSlice.transform.position += (Vector3.forward/10)*(i%piecesPerBatch) + new Vector3(-2,-1,0);		break;
					
				}
				
				batchTemp.PuzzleList.Add(playingSlice);
				
				batchIndex++;
				if(batchIndex == piecesPerBatch) 
				{
					batchIndex = 0;
					batchTemp.gameObject.SetActive(false);
				}
			}
			batchTemp.gameObject.SetActive(false);			//to prevent the last batch
		}
		else
		{
			for(int i=0;i<numOfPieces;i++)
			{
				completionSlices[i] = Instantiate(completionPiecePref) as CompletionPiece;
				completionSlices[i].name = "Completion Field " + i;
				completionSlices[i].transform.parent = completionPieceHolder.transform;
				completionSlices[i].transform.localPosition = Vector3.zero;
				completionSlices[i].CreatePiece(objectPuzzle.SpriteList[i]);
				
				if(batchIndex == 0)
				{
					batchTemp = Instantiate(batchPref) as Batch;
					batchTemp.name = "Batch " + i/piecesPerBatch;
					batchTemp.transform.parent = objectPuzzle.transform;
					batchTemp.transform.localPosition =  new Vector3(20.0f,0.0f,0.0f);
					batchList.Enqueue(batchTemp);
				}
				
				ObjectPuzzlePiece playingSlice = null;
				
				playingSlice		= Instantiate(puzzlePiecePref) as ObjectPuzzlePiece;
				playingSlice.name	= "Playing Piece " + i;
				playingSlice.CreatePiece(objectPuzzle.SpriteList[i]);
				playingSlice.PlacementPiece = completionSlices[i];
				playingSlice.transform.parent = batchTemp.transform;
				playingSlice.transform.localPosition = Vector3.zero;
				playingSlice.transform.rotation = Quaternion.Euler( currentRotation );
				switch(i%piecesPerBatch)
				{
				case 0:		playingSlice.transform.position += (Vector3.forward/10)*(i%piecesPerBatch) + new Vector3(-2,3,0);		break;
				case 1:		playingSlice.transform.position += (Vector3.forward/10)*(i%piecesPerBatch) + new Vector3(2,0,0);		break;
				case 2:		playingSlice.transform.position += (Vector3.forward/10)*(i%piecesPerBatch) + new Vector3(-2,-1,0);		break;
				}
				playingSlice.IsClickable = false;
				batchTemp.PuzzleList.Add(playingSlice);
				
				batchIndex++;
				if(i==0)
				{
					batchIndex = 0;
					batchTemp.gameObject.SetActive(false);
				}
				if(batchIndex == piecesPerBatch) 
				{
					batchIndex = 0;
					batchTemp.gameObject.SetActive(false);
				}
			}
			batchTemp.gameObject.SetActive(false);			//to prevent the last batch
		}
		objectPuzzle.SlidePuzzle();
	}



	/// <summary>
	/// Dequeues the next Batch of Object Puzzle from batch queue.
	/// </summary>
	public void DequeueBatch()
	{
		if(batchList.Count > 0)
		{
			currentBatch = batchList.Dequeue();
			currentBatch.gameObject.SetActive(true);
			currentBatch.StartBatch();

			
			
			UpdatePlayingPieces();
		}
		else
		{
			//objectPuzzle.StartAnimation();
			objectPuzzle.StartGlow();
		}
	}

	public void PuzzleAnimationCompleted()
	{
		//Debug.Log("Complete Puzzle!!");
		hintIdleNotPlaceTimer.enabled	= false;
		hintTouchNotPlaceTimer.enabled	= false;

		objectPuzzle.PuzzleCompleted = true;
		objectPuzzle.ObjectAlbumImage.SlideIn();

		UIManager.Instance.Blur.FadeIn();
		GameManager.Instance.UpdateScore(totalHintsRecieved,numOfPieces);
		starRank.ShowStars((byte)(GameManager.Instance.CurrentPlayerDifficulty+1));
		totalHintsRecieved = 0;
	}

	public void SideOut()
	{
		if(batchList.Count > 0) return;

		gameObject.Audio().clip = clips[Random.Range(0,1)];
		gameObject.Audio().Play();

		objectPuzzle.SlideTo.IsInverse = true;
		objectPuzzle.SlidePuzzle();
		UIManager.Instance.Blur.FadeOut();
	}

	/// <summary>
	/// Gets the playing puzzle.
	/// </summary>
	/// <value>The playing puzzle.</value>
	public ObjectPuzzle PlayingPuzzle
	{
		set { objectPuzzle = value; }
		get { return objectPuzzle; }
	}

	/// <summary>
	/// Gets the current batch.
	/// </summary>
	/// <value>The current batch.</value>
	public Batch CurrentBatch
	{
		get { return currentBatch; }
	}

	public void PieceSolved(ObjectPuzzlePiece _piece)
	{
		currentBatch.PieceSolved(_piece);
		hintsRecieved = 0;
	}

	public void BatchSolved()
	{
		ResetAndClearAllHintTimer();

		if(batchList.Count == 0)
		{
			FileManager.Instance.EndInteractiveTimer();
			PointsManager.Instance.ObjectPuzzleComplete();
			SoundManager.Instance.PlayObjectPuzzleComplete();
		}
		else
		{
			PointsManager.instance.BatchComplete(piecesPerBatch,numOfPieces);
			SoundManager.Instance.PlayBatchComplete();
		}

		//do stuff
		hintIdleNotPlaceTimer.enabled		= false;
		hintTouchNotPlaceTimer.enabled 		= false;
	}

	public void NextBatch()
	{
		DequeueBatch();
	}

	public void PauseHints()
	{
		for(int i=0;i<hintsRecieved;i++)
		{
			hintList[i].PauseHint();
		}
	}

	public void ResumeHints()
	{
		for(int i=0;i<hintsRecieved;i++)
		{
			hintList[i].ResumeHint();
		}
	}



	public void StartHintTimer(int _index)
	{
		hintIdleNotPlaceTimer.enabled = true;
		hintIdleNotPlaceTimer.ResetTimer();
		
		hintTouchNotPlaceTimer.enabled = true;
		hintTouchNotPlaceTimer.ResetTimer();

		hintList[_index].StartHint((ClickablePieceBase)currentBatch.RandomClickablePiece());
	}

	public void StartHintTimer()
	{
		hintIdleNotPlaceTimer.enabled = true;
		hintIdleNotPlaceTimer.ResetTimer();

		hintTouchNotPlaceTimer.enabled = true;
		hintTouchNotPlaceTimer.ResetTimer();
	}

	public void ResetIdleTiming()
	{
		hintIdleNotPlaceTimer.ResetTimer();
	}

	
	public void ResetHint(int _index)
	{
		hintList[_index].ClearHint();
	}

	public void ResetAndClearAllHintTimer()
	{
		hintIdleNotPlaceTimer.ResetTimer();
		hintTouchNotPlaceTimer.ResetTimer();
		for(int i=0;i < hintList.Length;i++)
		{
			hintList[i].ClearHint();
		}
	}

	public void Pause()
	{
		hintIdleNotPlaceTimer.IsPause 	= true;
		hintTouchNotPlaceTimer.IsPause	= true;

	}
	public void Resume()
	{
		hintIdleNotPlaceTimer.IsPause 		= false;
		hintTouchNotPlaceTimer.IsPause		= false;

	}
	private void HandleIdleHintRunning(float _elapse)
	{
		if(hintsRecieved >= hintTimingList.Length) return;
		if(_elapse > hintTimingList[hintsRecieved].idleNotPlaceTiming)
		{
	
			hintList[hintsRecieved].StartHint((ClickablePieceBase)currentBatch.RandomClickablePiece());

			hintsRecieved++;
			totalHintsRecieved++;


			hintTouchNotPlaceTimer.ResetTimer();
			hintIdleNotPlaceTimer.ResetTimer();
		}


	}

	private void HandleTouchHintRunning(float _elapse)
	{
		if(hintsRecieved >= hintTimingList.Length) return;
		if(_elapse > hintTimingList[hintsRecieved].touchNotPlaceTiming && hintTimingList[hintsRecieved].touchNotPlaceTiming != 0 && GameManager.Instance.CurrentPlayerState == PlayerState.touchNotMove)
		{
			hintList[hintsRecieved].StartHint((ClickablePieceBase)currentBatch.RandomClickablePiece());
			totalHintsRecieved++;
			hintsRecieved++;
			
			
			hintIdleNotPlaceTimer.ResetTimer();
			hintTouchNotPlaceTimer.ResetTimer();
		}
	}


	public void UpdatePlayingPieces()
	{
		PlayerPrefs.SetFloat("DistanceChecker",playingPieceDistanceChecker);
		if(objectPuzzle != null)
		{
			puzzlePiecePref.UpdateCheckDistance(playingPieceDistanceChecker);
			if(currentBatch != null)
			{
				for(int i=0;i<currentBatch.PuzzleList.Count;i++)
				{
					ObjectPuzzlePiece temp = (ObjectPuzzlePiece)currentBatch.PuzzleList[i];
					temp.UpdateCheckDistance(playingPieceDistanceChecker);
				}
			}
		}
	}
	

	public uint NumOfPiece			 { 	get { return numOfPieces;			}	}
	public int PiecePerBatch		 {	get { return piecesPerBatch; 			}	}
	public float PlayingPieceDistanceChecker { 	set { playingPieceDistanceChecker = value;	}	}

	public Quaternion LowRotation()
	{
		return Quaternion.Euler(new Vector3(0.0f,0.0f,rotationDifficulty[0].lowRange.z+rotationDifficulty[0].highRange.z));
	}

	public int HintsRecieved	{	get { return hintsRecieved; }	}

	public Vector3 CurrentRotation 
	{
		set { currentRotation = value; 	}
		get { return currentRotation; 	}
	}

}