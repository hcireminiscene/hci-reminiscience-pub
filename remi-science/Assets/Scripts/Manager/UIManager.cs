﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

public class UIManager : Singleton<UIManager>
{
	[SerializeField] protected Image[] 	 uiSplash;
	[SerializeField] protected ImageFader uiBlur;
	[SerializeField] protected SlideTo[] coinBags;

	[SerializeField] protected Button		gameButton;
	[SerializeField] protected Button		graphButton;

	[SerializeField] private Color startColor;
	[SerializeField] private Color endColor;
	private MoneyCounterText[] moneyCounters;

	private bool		stuffToMoveStarted;

	private WaitTimer	fadeTimer;
	private int 		splashIndex;

	private void Start()
	{
		if(!PlayerPrefs.HasKey("DistanceChecker"))
		{
			PlayerPrefs.SetFloat("DistanceChecker",0.5f);
		}

		ObjectPuzzleManager.instance.UpdatePlayingPieces();

		for(int i=0;i<coinBags.Length;i++)
		{
			coinBags[i].ReachPosition += HandleReachPosition;
		}

		moneyCounters = new MoneyCounterText[coinBags.Length];
		for(int i=0;i<coinBags.Length;i++)
		{
			moneyCounters[i] = coinBags[i].GetComponentInChildren<MoneyCounterText>();
		}

	}

	public void HideButtons()
	{
		graphButton.gameObject.SetActive(false);
		gameButton.gameObject.SetActive(false);
	}

	public void ShowButtons()
	{
		graphButton.gameObject.SetActive(true);
		gameButton.gameObject.SetActive(false);
	}



	private void HandleReachPosition()
	{
		TaskEvent -= coinBags[0].Slide;
		TaskEvent -= coinBags[1].Slide;
		StopCoroutine("SlideTask");
	}

	public void SlideIn(int _coinBagIndex)
	{
		coinBags[_coinBagIndex].IsInverse = false;
		coinBags[_coinBagIndex].Slide();
	}
	public void SlideOut(int _coinBagIndex)
	{
		coinBags[_coinBagIndex].IsInverse = true;
		coinBags[_coinBagIndex].Slide();
	}

	public ImageFader Blur
	{
		get { return uiBlur; }
	}

	private delegate void TaskDelegate();
	private event TaskDelegate TaskEvent;


	public void UpdateMoneyCounter(uint _value,int _moneyBagIndex)
	{
		for(int i=0;i<moneyCounters.Length;i++)
		{
			moneyCounters[i].UpdateText(_value);
		}
		moneyCounters[_moneyBagIndex].StartTimer();
	}

}
