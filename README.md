# Remini-Science #

This private project is for University of Glasgow Singapore, HCI - Level 4 By Team 4. 

//Add Abstract Here about the project

# Setup #
The application is run on Unity 5.6.\* and above. Ideally do stick to Unity 5 and above as we are Sprite.OverrideGeometry() See (<https://docs.unity3d.com/560/Documentation/ScriptReference/Sprite.OverrideGeometry.html>) to generate shapes for the Object Puzzles using a set templates (in CSV format) that we have created using Photoshop. 

When you run the application in Unity Editor, for the first run will take some time (around 1hr+) as we have a lot of frame-based animation (and they are quite large in terms of size) that will be packed together using Unity SpritePacker and a Sprite Packer Tool that we have made to help us organized the images.

# Architecture/Design #
We mainly used Unity recommended approach (Component-Based Design) to develop this application. We will not be using MVC component-based design as it is more specifically used for Web Development. Instead we designed the application in such a way that a single GameObject may have multiple small components that corresponds to how that GameObject is supposed to run. (e.g. to make a Sprite Glow, we will have a Timer Component and a SpriteGlow Component ) 

We are also using Singleton design for our Managers classes as they will get really huge and complicated. This will help us make simple changes that affect that can affect the Overall Game, UI,Object and Scene Puzzles that is much harder to implement using a component-based approach. However, there are some drawbacks to this approach as we will be coupling the managers together which in turn oppose to how component-based approach is should be implemented. But given the scale of this project and time, we will only be producing a prototype of it and most of the codes would not be reused in other similar application.